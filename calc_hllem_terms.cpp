#include "hllem.hpp"

void calc_LR_delta(VectorXd Q, ftype sL, ftype sR, MatrixXd & L, MatrixXd & R, MatrixXd & delta){

	MatrixXd A=MatrixXd::Zero(Dim, Dim), A_v=MatrixXd::Zero(Dim, Dim), dQ_dV=MatrixXd::Zero(Dim, Dim), Lam=MatrixXd::Zero(Dim, Dim), abs_Lam=MatrixXd::Zero(Dim, Dim);
	VectorXd eigenvals(Dim);
	
	ftype a1=frac(Q,0), a2=frac(Q,1), r1=dens(Q,0), r2=dens(Q,1);
	if (MODEL_ID==7) {
		ftype p1=pressure(Q,0), p2=pressure(Q,1), p_I=pI(Q), u1=Q(1)/Q(0), u2=Q(5)/Q(4), u_I=uI(Q), c1=sqrt(a_a(Q,0)), c2=sqrt(a_a(Q,1)), ccI1=ccI(Q,0), ccI2=ccI(Q,1),  ksi1=c1*c1-(u1-u_I)*(u1-u_I), ksi2=c2*c2-(u2-u_I)*(u2-u_I);
		
		ftype aa1=a_a(Q,0), aa2=a_a(Q,1), 
		de_dr1=de_dr(Q,0), de_dr2=de_dr(Q,1),
		de_dp1=de_dp(Q,0), de_dp2=de_dp(Q,1),
		e1=ene(Q,0), e2=ene(Q,1);
		
		A_v << 
		u1, r1   , 0    , r1*(u1-u_I)/a1     , 0 , 0    , 0  ,
		0 , u1   , 1./r1, (p1-p_I)/(a1*r1)   , 0 , 0    , 0  ,
		0 ,r1*aa1, u1   ,r1*ccI1*(u1-u_I)/a1 , 0 , 0    , 0  ,
		0 ,  0   ,   0  ,     u_I            , 0 , 0    , 0  ,
		0 ,  0   ,   0  ,-r2*(u2-u_I)/a2     , u2,r2    , 0  ,
		0 ,  0   ,   0  ,-(p2-p_I)/(a2*r2)   , 0 ,u2    ,1/r2,
		0 ,  0   ,   0  ,-r2*ccI2*(u2-u_I)/a2, 0 ,r2*aa2,  u2;
		
		dQ_dV << a1                ,   0    ,     0      ,   r1   , 0                          , 0      , 0          ,
		         a1*u1             ,a1*r1   ,     0      ,   r1*u1, 0                          , 0      , 0          , 
		a1*(e1+r1*de_dr1+0.5*u1*u1),a1*r1*u1,a1*r1*de_dp1,Q(2)/a1 , 0                          , 0      , 0          ,
		         0                 ,   0    ,     0      ,   1    , 0                          , 0      , 0          ,
		         0                 ,   0    ,     0      ,-r2     , a2                         , 0      , 0          , 
		         0                 ,   0    ,     0      ,-r2*u2  , a2*u2                      ,a2*r2   , 0          ,
		         0                 ,   0    ,     0      ,-Q(6)/a2, a2*(e2+r2*de_dr2+0.5*u2*u2),a2*r2*u2,a2*r2*de_dp2;
		
		A = dQ_dV * A_v * dQ_dV.inverse();
		
		EigenSolver<MatrixXd> es(A);
		R = es.eigenvectors().real(); L=R.inverse();
		MatrixXcd Lamc = es.eigenvalues().asDiagonal();
		Lam = Lamc.real(), abs_Lam=Lam.cwiseAbs();
		
	}
	if (MODEL_ID==6) {
		ftype p1=pressure(Q,0), p2=pressure(Q,1), c1=sqrt(a_a(Q,0)), c2=sqrt(a_a(Q,1)), c=c6(Q), u = vel(Q), Y1 = Q(0)/(Q(0)+Q(4)), Y2 = Q(4)/(Q(0)+Q(4)), H1 = ene(Q,0) + p1/dens(Q,0) + u*u/2, H2 = ene(Q,1) + p1/dens(Q,1) + u*u/2, k1=mygamma[0]-1, k2=mygamma[1]-1, Pi1=-dens(Q,0)*c1*c1 + p1*k1, Pi2=-dens(Q,1)*c2*c2 + p2*k2;
		
		R.row(0) << 0  ,0,0,0,1,0  ;
		R.row(1) << Y1 ,0,0,1,0,Y1 ;
		R.row(2) << Y2 ,0,1,0,0,Y2 ;
		R.row(3) << u-c,0,u,u,0,u+c;
		R.row(4) << Y1*(H1-u*c),-k2/k1, k2/k1*H2-c2*c2/k1,H1-c1*c1/k1,(Pi1-Pi2)/k1,Y1*(H1+u*c);
		R.row(5) << Y2*(H2-u*c),1, 0,0,0, Y2*(H2+u*c);
		
		dQ_dV << 
		0,1,0,0,0,0,
		0,0,0,1,0,0,
		0,0,0,0,1,0,
		1,0,0,0,0,0,
		0,0,1,0,0,0,
		0,0,0,0,0,1;
		
		R=dQ_dV*R;
		
		L=R.inverse();
		
		
		eigenvals << u-c, u, u, u, u, u+c;
		Lam = eigenvals.asDiagonal(); abs_Lam=Lam.cwiseAbs();
		
		
	}
	if (MODEL_ID==5) {
		ftype rho = a1*r1 + a2*r2, c = cw(Q), u = vel(Q);
		A_v << 
		u,rho,0,0,0,
		0,u,1/rho,0,0,
		0,rho*cw(Q)*cw(Q),u,0,0,
		0,0,0,u,0,
		0,K(Q),0,0,u;
		
		dQ_dV << 
		Q(0)/rho, 0 , 0, rho , 0,
		u, rho, 0, 0, 0,
		ene(Q)+u*u/2-(pressure(Q)*gmean(a1) + pimean(a1)) / rho, rho*u,gmean(a1),0,0,
		0,0,0,0,1,
		1-Q(0)/rho,0,0,-rho,0;
		
		A = dQ_dV * A_v * dQ_dV.inverse();
		
		EigenSolver<MatrixXd> es(A);
		R = es.eigenvectors().real(); L=R.inverse();
		MatrixXcd Lamc = es.eigenvalues().asDiagonal();
		Lam = Lamc.real(), abs_Lam=Lam.cwiseAbs();
		
// 		R.col(0) << 0,0,0,1,0;
// 		R.col(1) << 1,0,0,0,0;
// 		R.col(2) << 0,1,0,0,0;
// 		R.col(3) << 0,0,1,0,rmean*cw(Q);
// 		R.col(4) << 0,0,1,0,-rmean*cw(Q);
// 		L=R.inverse();
// 		eigenvals << u, u, u, u+c, u-c;
// 		Lam = eigenvals.asDiagonal(); abs_Lam=Lam.cwiseAbs();
	}
	
	delta=MatrixXd::Identity(Dim,Dim) - 0.5*(Lam-abs_Lam)* 1./(sL-1e-14) - 0.5*(Lam+abs_Lam)* 1./(sR+1e-14);
}

