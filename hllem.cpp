#include "hllem.hpp"

void S_LR_calc(VectorXd&  QL  , VectorXd&  QR  , ftype & sL, ftype & sR){
	sL=min(minmaxeigenvalue(QL,0) , minmaxeigenvalue( 0.5*(QL+QR) , 0) );
	if (sL>0) sL=0; 
	sR=max(minmaxeigenvalue(QR,1) , minmaxeigenvalue( 0.5*(QL+QR) , 1) );
	if(sR<0) sR=0;
}



/** Интегральное среднее B по пути (ломаная из двух прямых отрезков). Используем трехточечную Гауссову квадратуру. */
void calc_B_mean (VectorXd Qa, VectorXd Qb, MatrixXd& Bmean) {
	ftype gq_point[3]={-sqrt(3./5) , 0 , sqrt(3./5)};
	ftype weight[3]={5./18 , 4./9, 5./18};
	Bmean = MatrixXd::Zero(Dim, Dim);
	
    //calc_matrix_B(0.5*(Qa+Qb) , Bmean);
    //return;
	 
	for (int s=0; s<3; s++) {
		MatrixXd Bi = MatrixXd::Zero(Dim, Dim);
		ftype x=0.5*(gq_point[s]+1);
		calc_matrix_B(Qa*x + Qb*(1-x),Bi);
		Bmean += weight[s] * Bi; 
	}
	
}
/*
void calc_D (VectorXd QL, VectorXd QR, VectorXd& D, int plus, ftype min_err_in_iterations, int& its4conv) {
	VectorXd fL(Dim), fR(Dim) , Q_star(Dim), Q_star_new(Dim), Err(Dim), HLLEM_term=VectorXd::Zero(Dim);
	flux (QL,fL); flux(QR,fR);
	ftype sL=0, sR=0;	
	S_LR_calc(QL,QR,sL,sR);
	
	MatrixXd BL = MatrixXd::Zero(Dim, Dim);
	MatrixXd BR = MatrixXd::Zero(Dim, Dim);
	Q_star=0.5*(QL+QR);
	calc_B_mean (QL,Q_star,BL);
	calc_B_mean (Q_star,QR,BR);
	int maxits=100, it=0;//с правилом останова лучше
	//dtype conf=
	ftype it_err=100;// any big number
	its4conv=0;
	while (it_err > min_err_in_iterations) {
		Q_star_new = (QR*sR-QL*sL-fR+fL-(BL*(Q_star-QL)+BR*(QR-Q_star))) / (sR-sL);
		its4conv++;
		Err = Q_star_new-Q_star;
        it_err=Err.dot(Err);
		//fprintf(stderr, "%d %g\n", it, it_err );
		Q_star=Q_star_new;
		calc_B_mean (QL,Q_star,BL);
		calc_B_mean (Q_star,QR,BR);
		
	}
	//fprintf(stderr, "\n");
	
	#ifdef USEHLLEM
    calc_HLLEM_term(QL,QR,sL,sR,HLLEM_term);
	#endif
	
	if (!plus) D = -sL/(sR-sL) * ( fR-fL + BL*(Q_star-QL)+BR*(QR-Q_star) ) + sL*sR/(sR-sL) * (QR-QL) - HLLEM_term;
	else       D =  sR/(sR-sL) * ( fR-fL + BL*(Q_star-QL)+BR*(QR-Q_star) ) - sL*sR/(sR-sL) * (QR-QL) + HLLEM_term;
}
*/

void calc_Dhll(VectorXd& QL, VectorXd& QR, VectorXd& D, int plus, ftype min_err_in_iterations, int& its4conv, VectorXd& Q_star, int n) {
	VectorXd Q_mean(Dim), fL(Dim), fR(Dim), Q_star_new(Dim), Err(Dim), HLLEM_term = VectorXd::Zero(Dim);
	VectorXd g(Dim), tmp(Dim), xil(Dim), xir(Dim);
	flux(QL, fL); flux(QR, fR);
	ftype sL = 0, sR = 0;
	S_LR_calc(QL, QR, sL, sR);
	
	MatrixXd BL = MatrixXd::Zero(Dim, Dim);
	MatrixXd BR = MatrixXd::Zero(Dim, Dim);
	MatrixXd Jac = MatrixXd::Zero(Dim, Dim);
	MatrixXd Un =  MatrixXd::Identity(Dim, Dim); 
	
	
	calc_B_mean(QL, QR, BL);
	//Q_star=0.5*(QL+QR);
	Q_star = (QR * sR - QL * sL - fR + fL - BL * (QR - QL)) / (sR - sL);
	calc_B_mean(QL, Q_star, BL);
	calc_B_mean(Q_star, QR, BR);
	int maxits = 100, it = 0;//с правилом останова лучше

	ftype it_err = 100.0;// any big number
	ftype it_err2;
	its4conv = 0;
	
	//Newton iteration 
	ftype beta = 0.05;
	while (it_err > min_err_in_iterations) {
		#ifdef USENEWTONIT
		Jac = (Un + (BL - BR) / (sR - sL));
		g = Q_star - (QR * sR - QL * sL - fR + fL - (BL * (Q_star - QL) + BR * (QR - Q_star))) / (sR - sL);
		Q_star_new = Q_star - beta * Jac.inverse() * g;
		its4conv++;
		it_err = calc_err(Q_star_new, Q_star);
		#else // simple iteration
		Q_star_new = (QR*sR-QL*sL-fR+fL-(BL*(Q_star-QL)+BR*(QR-Q_star))) / (sR-sL);
		its4conv++;
		Err = Q_star_new-Q_star;
		it_err=Err.dot(Err);
		#endif
		//fprintf(stderr, "%d %g\n", it, it_err );
		Q_star=Q_star_new;
		calc_B_mean (QL,Q_star,BL);
		calc_B_mean (Q_star,QR,BR);
	}
	int error = 1;
	//if (!Check(Q_star, error)) std::cerr << "Check failed in iterations, error code " << error << ", n = " << n << std::endl;
	
	
	if (!plus) D = -sL / (sR - sL) * (fR - fL + BL * (Q_star - QL) + BR * (QR - Q_star)) + sL * sR / (sR - sL) * (QR - QL);
	else       D = sR / (sR - sL) * (fR - fL + BL * (Q_star - QL) + BR * (QR - Q_star)) - sL * sR / (sR - sL) * (QR - QL);
}



void calc_Dhllem(VectorXd& QL, VectorXd& QR, VectorXd& D, VectorXd& Q_star, int plus, ftype min_err_in_iterations, ftype* Alpha, int n) {
	#ifndef USEHLLEM
	return;
	#else
	VectorXd Q_mean=0.5*(QL+QR), fL(Dim), fR(Dim), Q_star_new(Dim), Err(Dim), HLLEM_term = VectorXd::Zero(Dim);
	VectorXd tmp(Dim), xil(Dim), xir(Dim);
	flux(QL, fL); flux(QR, fR);
	ftype sL = 0, sR = 0;
	S_LR_calc(QL, QR, sL, sR);
	
	MatrixXd BL = MatrixXd::Zero(Dim, Dim), BR = MatrixXd::Zero(Dim, Dim), Jac = MatrixXd::Zero(Dim, Dim), Un = MatrixXd::Zero(Dim, Dim), delta = MatrixXd::Zero(Dim, Dim), Left = MatrixXd::Zero(Dim, Dim), Right = MatrixXd::Zero(Dim, Dim);
	
	Alpha[n] = 1.0;
	//calc_delta(Q_mean,sL,sR,delta);
	//std::cerr << "delta = " << delta << std::endl;
	calc_LR_delta(Q_mean,sL,sR,Left,Right,delta);
	//std::cerr << "L = " << Left << std::endl;
	//std::cerr << "R = " << Right << std::endl;
	
	tmp = sL * sR / (sR - sL) * Right * delta * Left * (QR - QL);
	//std::cerr << "hllem_term ~ " << tmp << std::endl;
	xil = Q_star - tmp;
	xir = Q_star + tmp;
	
	
	int error;
	
	// variables to check
	
	int Np = 5;
	int par[Np]; // densities, alp, energies
	
	par[0] = 0;
	par[1] = 2;
	par[2] = 4;
	par[3] = 3;
	par[4] = Dim-1;
	
	for (int i=0;i<Np;i++)
	{
		if (xil(par[i]) < 0.0)  Alpha[n] = min(Alpha[n], Q_star(par[i]) / tmp(par[i]));
		if (xir(par[i]) < 0.0)  Alpha[n] = min(Alpha[n], -Q_star(par[i]) / tmp(par[i]));
	}
	
	if (xir(3) > 1.0)  Alpha[n] = min(Alpha[n], 1.0 / tmp(3) - Q_star(3) / tmp(3));
	if (xil(3) > 1.0)  Alpha[n] = min(Alpha[n], Q_star(3) / tmp(3) - 1.0 / tmp(3));
	
	
	Alpha[n] = max(0.0, Alpha[n]);
	Alpha[n] = 0.99 * Alpha[n];
	
	xil = Q_star - Alpha[n] * tmp;
	xir = Q_star + Alpha[n] * tmp;
	
	
	HLLEM_term = tmp;
	
	//if (!Check(xil, error))	std::cerr << "Check failed hllem xilm:scheme_step " << error << ", n = " << n << std::endl;
	//if (!Check(xir, error)) std::cerr << "Check failed hllem xirm:scheme_step " << error << ", n = " << n << std::endl;
	
	
	if (!plus) D = - HLLEM_term;
	else       D =   HLLEM_term;
	
	
	

#endif
}

/// альтернативная схема с проверкой наклонов (более устойчивая)
void euler_step(VectorXd& Unew, VectorXd& UC, VectorXd& UL, VectorXd& UR, 
	ftype tau_h, ftype dt, ftype h, int& its4conv, ftype *AlphaR, ftype * AlphaL, int n){
	VectorXd Dm(Dim), Dp(Dim);
	VectorXd Dmhllem(Dim), Dphllem(Dim);
	VectorXd QstarL(Dim), QstarR(Dim);
	ftype min_err_in_iterations=1e-20;
	//ftype min_err_in_iterations = 1e-15;
	int itm=0, itp=0;
	calc_Dhll (UL  , UC  ,  Dp , 1, min_err_in_iterations, itm, QstarL, n);
	calc_Dhll (UC  , UR  ,  Dm , 0, min_err_in_iterations, itp, QstarR,  n);
	int error = 1;
	#ifdef USEHLLEM
	calc_Dhllem(UL, UC, Dphllem, QstarL, 1, min_err_in_iterations, AlphaL, n);
	calc_Dhllem(UC, UR, Dmhllem, QstarR, 0, min_err_in_iterations, AlphaR, n);
	ftype alpha_min = min(AlphaL[n], AlphaR[n]);
	Unew = UC - dt/h * (Dp+Dm + alpha_min*(Dphllem + Dmhllem));
	
	if (!Check(Unew, error)) {
	//	std::cerr << "Check failed: scheme_step " << error <<  ", n = " << n << ", alpha_min = "<< alpha_min << std::endl;
		Unew = UC - dt / h * (Dp + Dm); // try HLL
	//	if (!Check(Unew, error)) std::cerr << "Check failed: scheme_step for hll " << error << std::endl;
	}
	
	#else
	Unew = UC - dt / h * (Dp + Dm); 
	// if (!Check(Unew, error)) std::cerr << "Check failed: scheme_step for hll " << error << ", n = " << n << std::endl;
	#endif
	its4conv = (itm+itp)/2+1;
}
				 
				 
/*
void scheme_step_old(VectorXd& Unew, VectorXd UC, VectorXd UL, VectorXd UR, ftype tau_h, int& its4conv){
	VectorXd Dm(Dim), Dp(Dim);
    ftype min_err_in_iterations=1e-20;
    int itm=0, itp=0;
	calc_D (UL  , UC  ,  Dp , 1, min_err_in_iterations, itm);
	calc_D (UC  , UR  ,  Dm , 0, min_err_in_iterations, itp);
	Unew = UC - tau_h * (Dp+Dm);
    its4conv = (itm+itp)/2+1;
}
*/
