#include <iostream>
#include <Eigen/Dense>
#include <omp.h>
#pragma once

using namespace Eigen;

typedef double ftype;


__inline__ ftype min(ftype a, ftype b) {return a<b?a:b;}
__inline__ ftype max(ftype a, ftype b) {return a>b?a:b;}


const int MODEL_ID = 6; // 3 - Eu, 4 - 4eq (deprecated), 5, 6 - Kapila5,6; 7 - BN.

/// if not defined, HLL flux will be used
//#define USEHLLEM
// if commented, 1 order scheme will be used
//#define SECONDORDERTVD
// if commented, simple iteration scheme will be used
#define USENEWTONIT

//#define RELAX

const int N = 5000;


const ftype lambda = 1e9; // coefficients of relaxation
const ftype nu = 100;

#define NUMF 2

//const ftype mygamma[NUMF]={1.4,1.4}, Pref[NUMF]={0,0};
const ftype mygamma[NUMF]={4.4,1.4}, Pref[NUMF]={6e8,0};
//const ftype mygamma[NUMF]={3,1.4}, Pref[NUMF]={100,0};

ftype gmean(ftype) ;
ftype pimean(ftype);

/** Уравнение сотояния (расчет давления) */
ftype pressure (VectorXd Q, int phase=0);

ftype frac(VectorXd Q, int phase);
ftype de_dr(VectorXd Q, int phase=0);
ftype de_dp(VectorXd Q, int phase=0);
ftype a_a  (VectorXd Q, int phase=0);
ftype sound_speed(VectorXd Q, int phase=0);
ftype dens(VectorXd Q, int phase=0);
ftype vel(VectorXd Q, int phase=0);
ftype ene(VectorXd Q, int phase=0);
ftype e_p(ftype pres, ftype dens, int phase=0, ftype fr=0);

const int Dim = MODEL_ID;
ftype pI (VectorXd Q);
ftype uI (VectorXd Q);
ftype ccI (VectorXd Q, int phase);
ftype c6 (VectorXd Q);
ftype p5 (VectorXd Q);
ftype K (VectorXd Q);
ftype cw (VectorXd Q);
ftype minmaxeigenvalue(VectorXd Q, int mx);
void S_LR_calc(VectorXd&  QL  , VectorXd&  QR  , ftype & sL, ftype & sR);
void flux (VectorXd  Q  ,  VectorXd& F );
void calc_matrix_A (VectorXd Q, MatrixXd& A);
void calc_matrix_B (VectorXd Q, MatrixXd& B);
void calc_B_mean (VectorXd Qa, VectorXd Qb, MatrixXd& Bmean);
void calc_LR_delta(VectorXd Q, ftype, ftype, MatrixXd & L, MatrixXd & R, MatrixXd & delta);
void calc_HLLEM_term (VectorXd QL, VectorXd QR, ftype sL, ftype sR, VectorXd& H);
//void calc_D (VectorXd QL, VectorXd QR, VectorXd& D, int plus, ftype , int& );
void calc_Dhll(VectorXd& QL, VectorXd& QR, VectorXd& D, int plus, ftype min_err_in_iterations, int& its4conv, VectorXd& Q_star, int n);
void calc_Dhllem(VectorXd& QL, VectorXd& QR, VectorXd& D, VectorXd& Q_star, int plus, ftype min_err_in_iterations, ftype* Alpha, int n);
//void scheme_step_old(VectorXd& Unew, VectorXd UC, VectorXd UL, VectorXd UR, ftype tau_h, int&);
void euler_step(VectorXd& Unew, VectorXd& UC, VectorXd& UL, VectorXd& UR,ftype tau_h, ftype dt, ftype h, int& its4conv, ftype *AlphaR, ftype * AlphaL, int n);
void source_step(VectorXd& UC, ftype dt, int n, int &it); 
ftype calc_err(VectorXd& U1, VectorXd& U2);

void write_data(double time, ftype* X, VectorXd* U, int N, ftype* AlphaR , ftype* AlphaL);
bool Check(VectorXd& U, int& error); //checking for conservative variables

//void calc_sorces_matexp(VectorXd& U, double dt, int n); // method 1 
void calc_sorces_analytical_split(VectorXd& U, double dt, int n); //method 2 
//void calc_sorces_analytical(VectorXd& U, double dt, double& dt_new, int n, int &it); // method 3
//void calc_sorces_boost_runge_kutta_dopri5(VectorXd& U, double dt, int n); // relaxation with boost
//void calc_sorces_boost_rosenbrock4(VectorXd& U, double dt, int n); // relaxation with boost 
//void calc_vel_equi(VectorXd& U); // equilibrium velocity relaxation 
//void calc_press_equi(VectorXd& U); // equilibrium pressure relaxation
//ftype calc_max_equi_phi(VectorXd& U); // equilibrium volume fraction
void calc_analitical_velocity_relax(VectorXd& U, double dt);
void calc_analitical_pressure_relax(VectorXd& U, double dt, int n);

//void convertUToV(VectorXd& U, VectorXd& V); // convert conservative variables into primitive variables
//void convertVToU(VectorXd& U, VectorXd& V); // convert primitive variables into conservative variables
//void convertUToX(VectorXd& U, vector_type& V); // convert conservative variables into primitive variables (boost arrays)
//bool CheckV(VectorXd& V, VectorXd& U); //checking for primitive variables
//void calc_matrix(MatrixXd& A, VectorXd& V, VectorXd& U, double dt_ode);
//ftype calc_Jacob(MatrixXd& A, VectorXd& U, VectorXd& Uold, ftype dt);

struct numerical_method {
	VectorXd * U, * W, * slope;
	ftype *AlphaL, *AlphaR;
	ftype L, tau, h, CFL, time;
	char its[N];
	void init ( );
	void calc ()   ;
	void dQ (int, VectorXd& )  ;
	void global_time_step() ;
	void out ()    ;
	void turn_off();
};


inline ftype minmod (ftype x, ftype y) {
	if (x>=0 && y>=0) return min(x,y);
	if (x<0  && y<0)  return max(x,y);
	return 0;
}

