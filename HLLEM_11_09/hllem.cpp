#include <iostream>
#include <Eigen/Dense>
#include <unsupported/Eigen/MatrixFunctions>
#include "hllem.hpp"
#include <fstream> 
#include <limits>


# include "matrix_exponential.hpp"




/** Скорости распространения волн (собственные значения) */
ftype minmaxeigenvalue(VectorXd &Q, int mx) {
	// вычисление скорости звука (идеальный газ) 
//	ftype c1=sqrt( mygamma[0] * pressure(Q,0) * Q(Dim-1) / Q(0) ), c2=sqrt( mygamma[1] * pressure(Q,1) * (1-Q(Dim-1))/ Q(4) );
	ftype c1 = sound_speed(Q, 0);
	ftype c2 = sound_speed(Q, 1);
	ftype v1 = Q(1)/Q(0), v2 = Q(5)/Q(4);
	if (!mx)
		return min(v1-c1, v2-c2);
	return max(v1+c1, v2+c2);
}


void S_LR_calc(VectorXd&  QL  , VectorXd&  QR  , ftype & sL, ftype & sR){
	VectorXd tmp = 0.5 * (QL + QR);
	sL=min(minmaxeigenvalue(QL,0) , minmaxeigenvalue(tmp, 0) );
	if (sL>0) sL=0; 
	sR=max(minmaxeigenvalue(QR,1) , minmaxeigenvalue(tmp, 1) );
	if(sR<0) sR=0;
}


/** Уравнение сотояния (расчет давления) */
ftype pressure(VectorXd Q, int phase){ // может указатель добавить?  VectorXd &Q
	int shift=(Dim / 2)*phase;
	ftype frac=(phase==0?Q(Dim-1):1-Q(Dim-1));
	ftype dens=Q(shift)/frac;
	ftype u=Q(shift+1)/Q(shift);
	ftype v=Q(shift+2)/Q(shift);
	ftype p = (Q(shift+3)/frac - 0.5*dens*(u*u+v*v)) * (mygamma[phase]-1) - mygamma[phase]*Pref[phase];
	if (p < 0)
	{
	//	std::cout << "p<0" << std::endl;
		p = 0;
	}
	return p;
}

ftype dedrho(VectorXd &Q, int phase) { 
	int shift = (Dim / 2) * phase;
	ftype frac = (phase == 0 ? Q(Dim - 1) : 1 - Q(Dim - 1));
	ftype dens = Q(shift) / frac;
	ftype p = pressure(Q,phase);
	return -(p + mygamma[phase] * Pref[phase])/((mygamma[phase] - 1)*dens*dens);
}

ftype d2edrho2(VectorXd& Q, int phase) {
	int shift = (Dim / 2) * phase;
	ftype frac = (phase == 0 ? Q(Dim - 1) : 1 - Q(Dim - 1));
	ftype dens = Q(shift) / frac;
	ftype p = pressure(Q, phase);
	return 2 * (p + mygamma[phase] * Pref[phase]) / ((mygamma[phase] - 1) * dens * dens * dens);
}

ftype d2edrhodp(VectorXd& Q, int phase) {
	int shift = (Dim / 2) * phase;
	ftype frac = (phase == 0 ? Q(Dim - 1) : 1 - Q(Dim - 1));
	ftype dens = Q(shift) / frac;
	ftype p = pressure(Q, phase);
	return -1.0 / ((mygamma[phase] - 1) * dens * dens);
}


ftype dedp(VectorXd &Q, int phase) { 
	int shift = (Dim / 2) * phase;
	ftype frac = (phase == 0 ? Q(Dim - 1) : 1 - Q(Dim - 1));
	ftype dens = Q(shift) / frac;
	return 1.0 / ((mygamma[phase] - 1) * dens);
}

ftype d2edpdrho(VectorXd& Q, int phase) {
	int shift = (Dim / 2) * phase;
	ftype frac = (phase == 0 ? Q(Dim - 1) : 1 - Q(Dim - 1));
	ftype dens = Q(shift) / frac;
	return - 1.0 / ((mygamma[phase] - 1) * dens * dens);
}


ftype energy(ftype pres, ftype dens, int phase)
{
	return (pres + mygamma[phase] * Pref[phase]) / ((mygamma[phase] - 1) * dens);
}

ftype sound_speed(VectorXd& Q, int phase) { // может указатель добавить?  VectorXd &Q
	int shift = (Dim / 2) * phase;
	ftype frac = (phase == 0 ? Q(Dim - 1) : 1 - Q(Dim - 1));
	ftype dens = Q(shift) / frac;
	ftype p = pressure(Q, phase);
	ftype sound = (p / (dens * dens) - dedrho(Q, phase))/ dedp(Q, phase);
	return sqrt(sound);
}


ftype Ui(VectorXd& Q) {
	return Q(1) / Q(0);
}

ftype dUidu1(VectorXd& Q) {
	return 1;
}

ftype dUidu2(VectorXd& Q) {
	return 0;
}

ftype Pi(VectorXd& Q) {
	
	//ftype p1 = pressure(Q, 0);
	//ftype p2 = pressure(Q, 1);
	//ftype c1 = sound_speed(Q, 0);
	//ftype c2 = sound_speed(Q, 1);
	//ftype z1 = c1*Q(0) / Q(Dim - 1);
	//ftype z2 = c2*Q(4) / (1.0 - Q(Dim - 1));
	//ftype pi = (z1 * p1 + z2 * p2) / (z1 + z2);
	//if (pi < 0)
	//{
	//	pi = 0;
	//	//std::cout << "pi < 0" << std::endl;
	//	//p1 = pressure(Q, 0);
	//}
	ftype pi = pressure(Q, 1);
	return pi;

}

ftype dPidp1(VectorXd& Q) {
	return 0;
}

ftype dPida1(VectorXd& Q) {
	return 0;
}

ftype dPidp2(VectorXd& Q) {
	return 1;
}



ftype calc_err(VectorXd& U1, VectorXd& U2)
{
	VectorXd temp = U1 - U2;
	return	 temp.norm() / (U1.norm() + U2.norm() + 1.e-12);
}

ftype calc_err(VectorXd& U1, VectorXd& U2, int size,  int &i)
{
	ftype temp;
	ftype err_max = 0;
	ftype err_cur = 0;
	int imax = -1;
	for (int i = 0; i < size; i++)
	{
		temp = (U1(i) - U2(i));
		err_cur = temp*temp / (U1(i)* U1(i) + U2(i)*U2(i) + 1.e-12);
		if (err_cur > err_max)
		{
			err_max = err_cur;
			imax = i;
		}
	}
	i = imax;
	return	 err_max;
}


/** Консервативные члены */
void flux (VectorXd&  Q  ,  VectorXd& F ) {
	//F<<Q; return;
	int Sh = (Dim / 2);
	for (int phase=0; phase<2; phase++) {
		int shift=Sh*phase;
		ftype u=Q(shift+1)/Q(shift);
		ftype frac=(phase==0?Q(Dim-1):1-Q(Dim-1));
		for (int s=0; s<Sh; s++) F(s+shift)=Q(s+shift) * u;// * frac;
		ftype pre=pressure(Q , phase);
		F(1+shift)+=pre * frac;
		F(3+shift)+=u*pre * frac;
	}
	F(Dim-1)=0;
}

/** Матрица A(Q) -- задается в зависимости от решаемой задачи 
 * v *oid calc_matrix_A (VectorXd Q, MatrixXd& A) {
 * 
 * MatrixXd A = MatrixXd::Zero(Dim, Dim), B = MatrixXd::Zero(Dim, Dim);
 * calc_matrix_B (Q, B);
 * 
 * A+=B;
 * }
 */

/** Матрица B(Q) -- задается в зависимости от решаемой задачи. */
//void calc_matrix_B (VectorXd Q, MatrixXd& B) {
//	B=MatrixXd::Zero(Dim, Dim); // return;
//	ftype vi = Ui(Q);//
//	//ftype pi=pressure(Q,1);
//	ftype pi = Pi(Q); 
//	B.col(Dim-1) << 0 , -pi, 0, -pi*vi, 
//	                0 , pi , 0,  pi*vi, 
//                    vi;
//	return;
//}

void calc_matrix_B(VectorXd &Q, MatrixXd& B) {
	B = MatrixXd::Zero(Dim, Dim); // return;
	ftype vi = Ui(Q);//
	//ftype pi=pressure(Q,1);
	ftype pi = Pi(Q);
	B.col(Dim - 1) << 0, -pi, 0, -pi * vi,
		0, pi, 0, pi* vi,
		vi;
	return;
}
/** Интегральное среднее B по пути (ломаная из двух прямых отрезков). Используем трехточечную Гауссову квадратуру. */
void calc_B_mean (VectorXd &Qa, VectorXd &Qb, MatrixXd& Bmean) {
	ftype gq_point[3]={-sqrt(3./5) , 0 , sqrt(3./5)};
	ftype weight[3]={5./18 , 4./9, 5./18};
	Bmean = MatrixXd::Zero(Dim, Dim);
	VectorXd Qpth(Dim);
	
    //calc_matrix_B(0.5*(Qa+Qb) , Bmean);
    //return;
	// LINEAR FUNCTION
	/*for (int s=0; s<3; s++) {
		MatrixXd Bi = MatrixXd::Zero(Dim, Dim);
		ftype x=0.5*(gq_point[s]+1);
		Qpth = Qa * (1 - x) + Qb * x;
		calc_matrix_B(Qpth,Bi);
		Bmean += weight[s] * Bi; 
	}*/


	// SMOOTH STEP FUNCTION
	/*ftype div, dpsidx, c_coef, r_coef;
	c_coef = 0.5;
	r_coef = 20;
	for (int s = 0; s < 3; s++) {
		MatrixXd Bi = MatrixXd::Zero(Dim, Dim);
		ftype x = 0.5 * (gq_point[s] + 1);
		div = (exp(c_coef * r_coef) + exp(x * r_coef));
		dpsidx = r_coef * exp(r_coef * x) * exp(c_coef * r_coef) / (div * div);
		Qpth = Qa * exp(c_coef * r_coef) / div + Qb * exp(x * r_coef) / div;
		calc_matrix_B(Qpth, Bi);
		Bmean += weight[s] * dpsidx * Bi;
	}*/

	MatrixXd Bi = MatrixXd::Zero(Dim, Dim);
	Qpth = 0.5 * (Qa + Qb);
	calc_matrix_B(Qpth, Bi);
	Bmean  = Bi;

}


void calc_D (VectorXd& QL, VectorXd& QR, VectorXd& D, int plus, ftype min_err_in_iterations, int& its4conv, ftype *Alpha,  int n) {
	VectorXd Q_mean(Dim), fL(Dim), fR(Dim) , Q_star(Dim), Q_star_new(Dim), Err(Dim), HLLEM_term=VectorXd::Zero(Dim);
	VectorXd g(Dim), tmp(Dim), xil(Dim), xir(Dim);
	flux (QL,fL); flux(QR,fR);
	ftype sL=0, sR=0;	
	S_LR_calc(QL,QR,sL,sR);
	
	MatrixXd BL = MatrixXd::Zero(Dim, Dim);
	MatrixXd BR = MatrixXd::Zero(Dim, Dim);
	MatrixXd Jac = MatrixXd::Zero(Dim, Dim);
	MatrixXd Un = MatrixXd::Zero(Dim, Dim);
	for (int i = 0; i < Dim; i++)
	{
		for (int j = 0; j < Dim; j++)
		{
			if (i == j) Un(i, j) = 1;
		}
	}

	calc_B_mean(QL, QR, BL);
	//Q_star=0.5*(QL+QR);
	Q_star = (QR * sR - QL * sL - fR + fL - BL * (QR - QL)) / (sR - sL);
	calc_B_mean (QL,Q_star,BL);
	calc_B_mean (Q_star,QR,BR);
	int maxits=100, it=0;//с правилом останова лучше
	//dtype conf=
	ftype it_err=100.0;// any big number
	ftype it_err2;
	its4conv=0;
	//while (it_err > min_err_in_iterations) {
	//	Q_star_new = (QR*sR-QL*sL-fR+fL-(BL*(Q_star-QL)+BR*(QR-Q_star))) / (sR-sL);
	//	its4conv++;
	//	Err = Q_star_new-Q_star;
	//	it_err = calc_err(Q_star_new, Q_star);
 //       //it_err=Err.dot(Err);
	//	//fprintf(stderr, "%d %g\n", it, it_err );
	//	Q_star=Q_star_new;
	//	calc_B_mean (QL,Q_star,BL);
	//	calc_B_mean (Q_star,QR,BR);
	//	
	//}
	//Newton iteration 
	ftype beta = 0.05;
	while (it_err > min_err_in_iterations) {
		Jac = (Un + (BL - BR) / (sR - sL));
		g = Q_star - (QR * sR - QL * sL - fR + fL - (BL * (Q_star - QL) + BR * (QR - Q_star))) / (sR - sL);
		Q_star_new = Q_star - beta* Jac.inverse() * g;
		its4conv++;

		it_err = calc_err(Q_star_new, Q_star);
		Q_star = Q_star_new;
		calc_B_mean(QL, Q_star, BL);
		calc_B_mean(Q_star, QR, BR);
	}
	int error = 1;
	if (!Check(Q_star, error)) std::cout << "Check failed newton iteration:scheme_step " << error << std::endl;
	
	#ifdef USEHLLEM
	Q_mean = 0.5 * (QL + QR);
//	Q_mean = Q_star;
	ftype f1 = Q_mean(Dim - 1), f2 = 1 - f1,
		r1 = Q_mean(0) / f1, r2 = Q_mean(4) / f2,
		u1 = Q_mean(1) / Q_mean(0), u2 = Q_mean(5) / Q_mean(4),
		v1 = Q_mean(2) / Q_mean(0), v2 = Q_mean(6) / Q_mean(4),
		v1v1 = u1 * u1 + v1 * v1, v2v2 = u2 * u2 + v2 * v2,
		p1 = pressure(Q_mean, 0), p2 = pressure(Q_mean, 1),
		//c1c1=mygamma[0]*p1/r1, 
		//c2c2=mygamma[1]*p2/r2;
		c1c1 = sound_speed(Q_mean, 0),
		c2c2 = sound_speed(Q_mean, 1);
		c2c2 *= c2c2;
		c1c1 *= c1c1;
	ftype r1E1 = Q_mean(3) / f1,
		  r2E2 = Q_mean(7) / f2;

	ftype f1E1 = Q_mean(3) / r1, 
	      f2E2 = Q_mean(7) / r2;
	
	DiagonalMatrix<ftype, 5> Lam (u1,u1,u1,u2,u2),  abs_Lam (fabs(u1),fabs(u1),fabs(u1),fabs(u2),fabs(u2)) , Iden(1,1,1,1,1) , delta;
	delta=Iden - 0.5*(Lam-abs_Lam)*(1./(sL-1e-14)) - 0.5*(Lam+abs_Lam)*(1./(sR+1e-14));
	MatrixXd Left = MatrixXd::Zero(5, Dim), Right = MatrixXd::Zero(Dim,5), dQ_dV(Dim,Dim);
	
	Left<<0,0,1,0       ,0,0,0,0       ,0                ,
	1      ,0,0,-1./c1c1,0,0,0,0       ,(p2-p1)/(f1*c1c1),
	0      ,0,0,0       ,0,0,0,0       ,r2/(f2*((u1-u2)*(u1-u2)-c2c2)),
	0      ,0,0,0       ,0,0,1,0       ,    0,
	0      ,0,0,0       ,1,0,0,-1./c2c2,    0;
	Right<<0,1,0                                       ,0,0,
	       0,0,0                                       ,0,0,
	       1,0,0                                       ,0,0,
           0,0,f2*(p2-p1)*((u1-u2)*(u1-u2)-c2c2)/(f1*r2),0,0,
           0,0,(u1-u2)*(u1-u2)                         ,0,1,
	       0,0,c2c2*(u1-u2)/r2                         ,0,0,
	       0,0,0                                       ,1,0,
	       0,0,c2c2*(u1-u2)*(u1-u2)                    ,0,0,
	       0,0,f2*((u1-u2)*(u1-u2)-c2c2)/r2            ,0,0;
	dQ_dV<<f1   ,0       ,0       ,0                ,0          ,0       ,0       ,0                ,r1,
	f1*u1       ,f1*r1   ,0       ,0                ,0          ,0       ,0       ,0                ,r1*u1,
	f1*v1       ,0       ,f1*r1   ,0                ,0          ,0       ,0       ,0                ,r1*v1,
	0.5*f1*v1v1,f1*r1*u1,f1*r1*v1,f1/(mygamma[0]-1),0          ,0       ,0       ,0                 ,r1E1,
	0           ,0       ,0       ,0                ,f2         ,0       ,0       ,0                ,-r2,
	0           ,0       ,0       ,0                ,f2*u2      ,f2*r2   ,0       ,0                ,-r2*u2,
	0           ,0       ,0       ,0                ,f2*v2      ,0       ,f2*r2   ,0                ,-r2*v2,
	0           ,0       ,0       ,0                ,0.5*f2*v2v2,f2*r2*u2,f2*r2*v2,f2/(mygamma[1]-1),-r2E2,
	0           ,0       ,0       ,0                ,0          ,0       ,0       ,0                ,1;

	
	Right=dQ_dV*Right;
	Left=Left*dQ_dV.inverse();
	Alpha[n] = 1.0;
	tmp = Right * delta * Left*(QR - QL);
	xil = Q_star - tmp;
	xir = Q_star + tmp;

	

	for (int k = 0; k < Dim; k++)
	{
			if (xil(k) < 0.0)  Alpha[n] = min(Alpha[n], Q_star(k) / tmp(k));
			if (xir(k) < 0.0)  Alpha[n] = min(Alpha[n], - Q_star(k) / tmp(k));
	}
	if (xir(8) > 1.0)
	{
		Alpha[n] = min(Alpha[n], 1.0/ tmp(8)-Q_star(8) / tmp(8));
	}

	if (xil(8) > 1.0)
	{
		Alpha[n] = min(Alpha[n], Q_star(8) / tmp(8) - 1.0 / tmp(8));
	}

	Alpha[n] = max(0.0, Alpha[n]);
	Alpha[n] = 0.95 * Alpha[n];

	xil = Q_star - Alpha[n]*tmp;
	xir = Q_star + Alpha[n]*tmp;

	if (!Check(xil, error))
	{
		std::cout << "Check failed hllem xilm:scheme_step " << error << std::endl;
	}
	if (!Check(xir, error)) std::cout << "Check failed hllem xirm:scheme_step " << error << std::endl;
	
	HLLEM_term = Alpha[n]*sL * sR / (sR - sL) * tmp;

	#endif
	
	if (!plus) D = -sL/(sR-sL) * ( fR-fL + BL*(Q_star-QL)+BR*(QR-Q_star) ) + sL*sR/(sR-sL) * (QR-QL) - HLLEM_term;
	else       D =  sR/(sR-sL) * ( fR-fL + BL*(Q_star-QL)+BR*(QR-Q_star) ) - sL*sR/(sR-sL) * (QR-QL) + HLLEM_term;
	
	
	
	/*
	 *	Qmean=0.5*(QL+QR);
	 *	ftype umean = Qmean(1)/Qmean(0);
	 *	ftype phi=1;
	 *	MatrixXd L(1,Dim) , R(Dim,1);
	 *	ftype a = 0.5 * (sR - sL) ;
	 *	L/=(a*a);
	 *	R << 1 , umean , 0.5*umean*umean;
	 *	ftype delta=1 - 0.5*(umean-fabs(umean))/sL - 0.5*(umean+fabs(umean))/sR;  // (sR-sL) / (sR-sL + 2*fabs(umean));
	 *	F<<( sR*fL-sL*fR+(QR-QL - phi*R*delta*L*(QR-QL))*sL*sR )/(sR-sL) ; return; */
}


void calc_Dhll(VectorXd& QL, VectorXd& QR, VectorXd& D, int plus, ftype min_err_in_iterations, int& its4conv, VectorXd& Q_star, int n) {
	VectorXd Q_mean(Dim), fL(Dim), fR(Dim), Q_star_new(Dim), Err(Dim), HLLEM_term = VectorXd::Zero(Dim);
	VectorXd g(Dim), tmp(Dim), xil(Dim), xir(Dim);
	flux(QL, fL); flux(QR, fR);
	ftype sL = 0, sR = 0;
	S_LR_calc(QL, QR, sL, sR);

	MatrixXd BL = MatrixXd::Zero(Dim, Dim);
	MatrixXd BR = MatrixXd::Zero(Dim, Dim);
	MatrixXd Jac = MatrixXd::Zero(Dim, Dim);
	MatrixXd Un =  MatrixXd::Identity(Dim, Dim); 


	calc_B_mean(QL, QR, BL);
	//Q_star=0.5*(QL+QR);
	Q_star = (QR * sR - QL * sL - fR + fL - BL * (QR - QL)) / (sR - sL);
	calc_B_mean(QL, Q_star, BL);
	calc_B_mean(Q_star, QR, BR);
	int maxits = 100, it = 0;//с правилом останова лучше
	//dtype conf=
	ftype it_err = 100.0;// any big number
	ftype it_err2;
	its4conv = 0;
	//while (it_err > min_err_in_iterations) {
	//	Q_star_new = (QR*sR-QL*sL-fR+fL-(BL*(Q_star-QL)+BR*(QR-Q_star))) / (sR-sL);
	//	its4conv++;
	//	Err = Q_star_new-Q_star;
	//	it_err = calc_err(Q_star_new, Q_star);
 //       //it_err=Err.dot(Err);
	//	//fprintf(stderr, "%d %g\n", it, it_err );
	//	Q_star=Q_star_new;
	//	calc_B_mean (QL,Q_star,BL);
	//	calc_B_mean (Q_star,QR,BR);
	//	
	//}
	//Newton iteration 
	ftype beta = 0.05;
	while (it_err > min_err_in_iterations) {
		Jac = (Un + (BL - BR) / (sR - sL));
		g = Q_star - (QR * sR - QL * sL - fR + fL - (BL * (Q_star - QL) + BR * (QR - Q_star))) / (sR - sL);
		Q_star_new = Q_star - beta * Jac.inverse() * g;
		its4conv++;

		it_err = calc_err(Q_star_new, Q_star);
		Q_star = Q_star_new;
		calc_B_mean(QL, Q_star, BL);
		calc_B_mean(Q_star, QR, BR);
	}
	int error = 1;
	if (!Check(Q_star, error)) std::cout << "Check failed newton iteration:scheme_step " << error << std::endl;


	if (!plus) D = -sL / (sR - sL) * (fR - fL + BL * (Q_star - QL) + BR * (QR - Q_star)) + sL * sR / (sR - sL) * (QR - QL);
	else       D = sR / (sR - sL) * (fR - fL + BL * (Q_star - QL) + BR * (QR - Q_star)) - sL * sR / (sR - sL) * (QR - QL);
}



void calc_Dhllem(VectorXd& QL, VectorXd& QR, VectorXd& D, VectorXd& Q_star, int plus, ftype min_err_in_iterations, int& its4conv, ftype* Alpha, int n) {
	VectorXd Q_mean(Dim), fL(Dim), fR(Dim), Q_star_new(Dim), Err(Dim), HLLEM_term = VectorXd::Zero(Dim);
	VectorXd g(Dim), tmp(Dim), xil(Dim), xir(Dim);
	flux(QL, fL); flux(QR, fR);
	ftype sL = 0, sR = 0;
	S_LR_calc(QL, QR, sL, sR);

	MatrixXd BL = MatrixXd::Zero(Dim, Dim);
	MatrixXd BR = MatrixXd::Zero(Dim, Dim);
	MatrixXd Jac = MatrixXd::Zero(Dim, Dim);
	MatrixXd Un = MatrixXd::Zero(Dim, Dim);
	

#ifdef USEHLLEM
	Q_mean = 0.5 * (QL + QR);
	//	Q_mean = Q_star;
	ftype f1 = Q_mean(Dim - 1), f2 = 1 - f1,
		r1 = Q_mean(0) / f1, r2 = Q_mean(4) / f2,
		u1 = Q_mean(1) / Q_mean(0), u2 = Q_mean(5) / Q_mean(4),
		v1 = Q_mean(2) / Q_mean(0), v2 = Q_mean(6) / Q_mean(4),
		v1v1 = u1 * u1 + v1 * v1, v2v2 = u2 * u2 + v2 * v2,
		p1 = pressure(Q_mean, 0), p2 = pressure(Q_mean, 1),
		c1c1 = sound_speed(Q_mean, 0),
		c2c2 = sound_speed(Q_mean, 1);
	c2c2 = c2c2* c2c2;
	c1c1 = c1c1* c1c1;
	ftype r1E1 = Q_mean(3) / f1,
		r2E2 = Q_mean(7) / f2;

	ftype f1E1 = Q_mean(3) / r1,
		f2E2 = Q_mean(7) / r2;

	DiagonalMatrix<ftype, 5> Lam(u1, u1, u1, u2, u2), abs_Lam(fabs(u1), fabs(u1), fabs(u1), fabs(u2), fabs(u2)), Iden(1, 1, 1, 1, 1), delta;
	delta = Iden - 0.5 * (Lam - abs_Lam) * (1. / (sL - 1e-14)) - 0.5 * (Lam + abs_Lam) * (1. / (sR + 1e-14));
	MatrixXd Left = MatrixXd::Zero(5, Dim), Right = MatrixXd::Zero(Dim, 5), dQ_dV(Dim, Dim);

	Left << 0, 0, 1, 0, 0, 0, 0, 0, 0,
		1, 0, 0, -1. / c1c1, 0, 0, 0, 0, (p2 - p1) / (f1 * c1c1),
		0, 0, 0, 0, 0, 0, 0, 0, r2 / (f2 * ((u1 - u2) * (u1 - u2) - c2c2)),
		0, 0, 0, 0, 0, 0, 1, 0, 0,
		0, 0, 0, 0, 1, 0, 0, -1. / c2c2, 0;
	Right << 0, 1, 0, 0, 0,
		0, 0, 0, 0, 0,
		1, 0, 0, 0, 0,
		0, 0, f2* (p2 - p1)* ((u1 - u2) * (u1 - u2) - c2c2) / (f1 * r2), 0, 0,
		0, 0, (u1 - u2)* (u1 - u2), 0, 1,
		0, 0, c2c2* (u1 - u2) / r2, 0, 0,
		0, 0, 0, 1, 0,
		0, 0, c2c2* (u1 - u2)* (u1 - u2), 0, 0,
		0, 0, f2* ((u1 - u2) * (u1 - u2) - c2c2) / r2, 0, 0;
	dQ_dV << f1, 0, 0, 0, 0, 0, 0, 0, r1,
		f1* u1, f1* r1, 0, 0, 0, 0, 0, 0, r1* u1,
		f1* v1, 0, f1* r1, 0, 0, 0, 0, 0, r1* v1,
		0.5 * f1 * v1v1, f1* r1* u1, f1* r1* v1, f1 / (mygamma[0] - 1), 0, 0, 0, 0, r1E1,
		0, 0, 0, 0, f2, 0, 0, 0, -r2,
		0, 0, 0, 0, f2* u2, f2* r2, 0, 0, -r2 * u2,
		0, 0, 0, 0, f2* v2, 0, f2* r2, 0, -r2 * v2,
		0, 0, 0, 0, 0.5 * f2 * v2v2, f2* r2* u2, f2* r2* v2, f2 / (mygamma[1] - 1), -r2E2,
		0, 0, 0, 0, 0, 0, 0, 0, 1;


	Right = dQ_dV * Right;
	Left = Left * dQ_dV.inverse();
	Alpha[n] = 1.0;
	tmp = Right * delta * Left * (QR - QL);
	xil = Q_star - tmp;
	xir = Q_star + tmp;

// variables to check

    int Np = 5;
	int par[Np];

	par[0] = 0;
	par[1] = 3;
	par[2] = 4;
	par[3] = 7;
	par[4] = 8; 

	for (int i=0;i<Np;i++)
	{
		if (xil(par[i]) < 0.0)  Alpha[n] = min(Alpha[n], Q_star(par[i]) / tmp(par[i]));
		if (xir(par[i]) < 0.0)  Alpha[n] = min(Alpha[n], -Q_star(par[i]) / tmp(par[i]));
	}

	if (xir(8) > 1.0)  Alpha[n] = min(Alpha[n], 1.0 / tmp(8) - Q_star(8) / tmp(8));
	if (xil(8) > 1.0)  Alpha[n] = min(Alpha[n], Q_star(8) / tmp(8) - 1.0 / tmp(8));


	Alpha[n] = max(0.0, Alpha[n]);
	Alpha[n] = 0.99 * Alpha[n];

	xil = Q_star - Alpha[n] * tmp;
	xir = Q_star + Alpha[n] * tmp;

	int error;
	if (!Check(xil, error))	std::cout << "Check failed hllem xilm:scheme_step " << error << std::endl;
	if (!Check(xir, error)) std::cout << "Check failed hllem xirm:scheme_step " << error << std::endl;

	HLLEM_term = sL * sR / (sR - sL) * tmp;

#endif

	if (!plus) D = - HLLEM_term;
	else       D =   HLLEM_term;



	/*
	 *	Qmean=0.5*(QL+QR);
	 *	ftype umean = Qmean(1)/Qmean(0);
	 *	ftype phi=1;
	 *	MatrixXd L(1,Dim) , R(Dim,1);
	 *	ftype a = 0.5 * (sR - sL) ;
	 *	L/=(a*a);
	 *	R << 1 , umean , 0.5*umean*umean;
	 *	ftype delta=1 - 0.5*(umean-fabs(umean))/sL - 0.5*(umean+fabs(umean))/sR;  // (sR-sL) / (sR-sL + 2*fabs(umean));
	 *	F<<( sR*fL-sL*fR+(QR-QL - phi*R*delta*L*(QR-QL))*sL*sR )/(sR-sL) ; return; */
}

/// альтернативная схема с проверкой наклонов (более устойчивая)
void scheme_step(VectorXd& Unew, VectorXd& UC, VectorXd& UL, VectorXd& UR, 
				ftype tau_h, ftype dt, ftype h, int& its4conv, ftype *AlphaR, ftype * AlphaL, int n){
	VectorXd Dm(Dim), Dp(Dim);
	VectorXd Dmhllem(Dim), Dphllem(Dim);
	VectorXd QstarL(Dim), QstarR(Dim);
    //ftype min_err_in_iterations=1e-20;
	ftype min_err_in_iterations = 1e-15;
    int itm=0, itp=0;
	calc_Dhll (UL  , UC  ,  Dp , 1, min_err_in_iterations, itm, QstarL, n);
	calc_Dhll (UC  , UR  ,  Dm , 0, min_err_in_iterations, itp, QstarR,  n);
	calc_Dhllem(UL, UC, Dphllem, QstarL, 1, min_err_in_iterations, itm, AlphaL, n);
	calc_Dhllem(UC, UR, Dmhllem, QstarR, 0, min_err_in_iterations, itp, AlphaR, n);
	ftype alpha_min = min(AlphaL[n], AlphaR[n]);
#ifndef USEHLLEM
	alpha_min = 0;
#endif
	Unew = UC - dt/h * (Dp+Dm + alpha_min*(Dphllem + Dmhllem));
	int error = 1;
	if (!Check(Unew, error))
	{
#ifndef USEHLLEM
		std::cout << "Check failed: scheme_step " << error <<  ", n = " << n << ", alpha_min = "<< alpha_min << std::endl;
#endif
#ifdef USEHLLEM
		Unew = UC - dt / h * (Dp + Dm); // try HLL
		if (!Check(Unew, error))
		{
			std::cout << "Check failed: scheme_step for hll " << error << std::endl;
		}
#endif
	}
    its4conv = (itm+itp)/2+1;
}


// checking for conservative variables
bool Check(VectorXd& U, int& error) 
{
	for (int i = 0; i < Dim; i++)
	{
		if (isnan(U(i)))
		{
			error = -999;
			return false;
		}
	}

	if (U(8) > 1.0)
	{
		error = -8;
		return false;
	}
	else if (U(8) < 0)
	{
		error = -9;
		return false;

	}
	else if (U(0) < 0)
	{
		error = 0;
		return false;
	}
	else if (U(4) < 0)
	{
		error = -4;
		return false;
	}
	else if (U(3) < 0)
	{
		error = -3;
		return false;
	}
	else if (U(7) < 0)
	{
		error = -7;
		return false;
	}
	else return true;
}


std::string myreplace(std::string& s, const std::string& toReplace, const std::string& replaceWith)
{
	return(s.replace(s.find(toReplace), toReplace.length(), replaceWith));
}

void write_data(double time, ftype* X, VectorXd* U, int N, ftype*  AlphaR, ftype*  AlphaL)
{
	std::string name = "Prop_";
	std::string time_name = std::to_string(time);
	myreplace(time_name, ".", "p");
	name += time_name;
	name += ".dat";
	std::fstream plot(name, std::ios::out);
	ftype v1, v2, c1, c2, cpow2, cw;
	plot << "X, Pres1, Pres2, Phi, Ux1, Ux2, dens1, dens2, e1, e2, c1, c2, c, cw, odetime, ar, al " << std::endl;
	for (int i = 0; i < N; i++) {
		v1 = U[i][1] / U[i][0];
		v2 = U[i][5] / U[i][4];
		c1 = sound_speed(U[i], 0);
		c2 = sound_speed(U[i], 1);
		cpow2 = U[i][0] * c1 * c1 + U[i][4] * c2 * c2;
		cpow2 = cpow2 / (U[i][0] + U[i][4]);
		cw = U[i][8] / (U[i][0] / U[i][8] * c1 * c1) + (1.0 - U[i][8]) / (U[i][4] / (1.0 - U[i][8]) * c2 * c2);
		cw = 1.0 / ((U[i][0] + U[i][4]) * cw);
		plot << X[i] << ", " << pressure(U[i], 0) << ", " << pressure(U[i], 1) << ", "
			<< U[i][8] << ", " << v1 << ", " << v2 << ", " << U[i][0] / U[i][8] << ", "
			<< U[i][4] / (1.0 - U[i][8]) << ", " << U[i][3] / U[i][0] - 0.5 * v1 * v1 << ", "
			<< U[i][7] / U[i][4] - 0.5 * v2 * v2 << ", " << c1 << ", " << c2 << ", " << sqrt(cpow2) << ", " << sqrt(cw) << 
			", " << AlphaR[i] <<", " << AlphaL[i]<< std::endl;
	}
	
}


// ------------------------------Relaxation -------------------------------------

void scheme_source_step(VectorXd& UC, ftype dt, int n, int &it)
{
	calc_sorces_analytical_split(UC, dt, n); // method 2
	//calc_sorces_matexp(UC, dt, n); //method 1
	//calc_sorces_boost_rosenbrock4(UC, dt, n);
	//calc_sorces_boost_runge_kutta_dopri5(UC, dt, n);
	
	int error = 1;
	if (!Check(UC, error)) std::cout << "Check failed: scheme_source_step " << error << std::endl;
}


/*
void convertUToV(VectorXd& U, VectorXd& V)
{
	V(0) = U(1) / U(0); //u1
	V(1) = U(5) / U(4);// u2
	V(2) = pressure(U, 0); //p1
	V(3) = pressure(U, 1); // p2
	V(4) = U(8); //a1
}

void convertUToX(VectorXd& U, vector_type& V)
{
	V(0) = U(1) / U(0); //u1
	V(1) = U(5) / U(4);// u2
	V(2) = pressure(U, 0); //p1
	V(3) = pressure(U, 1); // p2
	V(4) = U(8); //a1
}


bool CheckV(VectorXd& V, VectorXd& U, int vDim)
{
	for (int i = 0; i < vDim; i++)
		if (isnan(V(i))) return false;

	if (V(4) > 1.0 - 1e-12 || V(4) < 1e-12)
		return false;

	ftype dens = U(0) / V(4);
	ftype ener1 = energy(V(2), dens, 0);

	dens = U(4) / (1.0 - V(4));
	ftype ener2 = energy(V(3), dens, 1);
	
	if (ener1 < 0.0 || ener2 < 0.0)
		return false;

	return true;

}

void convertXToU(VectorXd& U, vector_type& X)
{
	U(1) = U(0) * X(0);
	U(3) = U(0) * (energy(X(2), U(0) / X(4), 0) + 0.5* X(0)* X(0));
	U(5) = U(4) * X(1);
	U(7) = U(4) * (energy(X(3), U(4) / (1.0 - X(4)), 1) + 0.5 * X(1) * X(1));
	U(8) = X(4);
}


void convertVToU(VectorXd& U, VectorXd& V)
{
	U(1) = U(0) * V(0);
	ftype dens = U(0) / V(4);
	U(3) = U(0) * (energy(V(2), dens, 0) + 0.5 * V(0) * V(0));
	ftype phi = (1.0 - V(4));
	dens = U(4) / (1.0 - V(4));
	U(5) = U(4) * V(1);
	U(7) = U(4) * (energy(V(3), dens, 1) + 0.5 * V(1) * V(1));
	U(8) = V(4);
}


void calc_matrix(MatrixXd& A, VectorXd& V, VectorXd& U, double dt_ode)
{
	ftype a1rho1 = U(0);
	ftype a2rho2 = U(4);

	ftype rho1 = a1rho1 / V(4);
	ftype rho2 = a2rho2 / (1.0 - V(4));
	ftype dedr1 = dedrho(U, 0);
	ftype dedp1Inv = 1.0 / dedp(U, 0);
	ftype dedr2 = dedrho(U, 1);
	ftype dedp2Inv = 1.0 / dedp(U, 1);
	
	A(0,0) = -lambda / a1rho1 * dt_ode;
	A(0,1) = lambda / a1rho1 * dt_ode;
	A(0,2) = 0;
	A(0,3) = 0;
	A(0,4) = 0;

	A(1, 0) = lambda / a2rho2 * dt_ode;
	A(1, 1) = -lambda / a2rho2 * dt_ode;
	A(1, 2) = 0;
	A(1, 3) = 0;
	A(1, 4) = 0;

	A(2, 0) = lambda * (V(0) - Ui(U)) / a1rho1 * dedp1Inv * dt_ode;
	A(2, 1) = -lambda * (V(0) - Ui(U)) / a1rho1 * dedp1Inv * dt_ode;
	A(2, 2) = nu * (dedr1 * rho1 / V(4) - Pi(U) / a1rho1) * dedp1Inv * dt_ode;
	A(2, 3) = -nu * (dedr1 * rho1 / V(4) - Pi(U) / a1rho1) * dedp1Inv * dt_ode;
	A(2, 4) = 0;

	A(3, 0) = lambda * (Ui(U) - V(1)) / a2rho2 * dedp2Inv * dt_ode;
	A(3, 1) = -lambda * (Ui(U) - V(1)) / a2rho2 * dedp2Inv * dt_ode;
	A(3, 2) = nu * (-dedr2 * rho2 / (1.0 - V(4)) + Pi(U) / a2rho2) * dedp2Inv * dt_ode;
	A(3, 3) = -nu * (-dedr2 * rho2 / (1.0 - V(4)) + Pi(U) / a2rho2) * dedp2Inv * dt_ode;
	A(3, 4) = 0;

	A(4, 0) = 0;
	A(4, 1) = 0;
	A(4, 2) = nu * dt_ode;
	A(4, 3) = -nu * dt_ode;
	A(4, 4) = 0;

}*/

/*
ftype calc_Jacob_num(MatrixXd& A, VectorXd& V, VectorXd& U, VectorXd& Vold, double dt_ode, int vDim)
{
	MatrixXd A0(vDim, vDim), A1(vDim,vDim), A2(vDim, vDim), Ares(vDim,vDim);
	VectorXd Vnew(vDim), V1(vDim), U1(Dim), V2(vDim), U2(Dim), Vtemp(vDim);
	ftype eps = 1e-6;
	ftype delta;


    VectorXd Vlim_max(vDim); 
 	VectorXd Vlim_min(vDim); 

	Vlim_max<< DBL_MAX, DBL_MAX, DBL_MAX, DBL_MAX, 0.99999999999;
	Vlim_min<< DBL_MIN, DBL_MIN, 0.0, 0.0, 0.0;
	
	calc_matrix(A0, V, U, dt_ode);
	
    for (int par=0;par<vDim;par++)
	{
		delta = fabs(V(par)) * eps + eps;
		V1 = V;
		V2 = V;
		U1 = U;
		U2 = U;

		V1(par) = V(par) + delta;
		V2(par) = V(par) - delta; 
		if (V1(par) < Vlim_max(par) && V2(par) > Vlim_min(par))
		{		
			convertVToU(U1, V1);
			calc_matrix(A1, V1, U1, dt_ode);
			convertVToU(U2, V2);
			calc_matrix(A2, V2, U2, dt_ode);
			Ares = (A1 - A2)/(2*delta);
		}
		else if (V1(par) > Vlim_max(par))
		{
			convertVToU(U2, V2);
			calc_matrix(A2, V2, U2, dt_ode);
			Ares = (A0 - A2)/delta;	
		}
		else 
		{
			convertVToU(U1, V1);
			calc_matrix(A1, V1, U1, dt_ode);
			Ares = (A1 - A0) / delta;
		}

		Vtemp = Ares * Vold;
		for (int i = 0; i < vDim; i++)
				A(i, par) = -0.5*Vtemp(i);
		
		A(par, par) = 1.0 + A(par, par);
	}

	return A.norm();
}*/
/*
void calc_sorces_matexp(VectorXd& U, double dt, int n) //method 1
{
	//std::cout << "begin calc sources\n" << std::endl;
	//std::cout << "n = " << n << std::endl;

	ftype dt_ode = dt;
	ftype time = dt;
	ftype cur_time = 0;
	int count;
	ftype err;
	bool status;
	VectorXd V(vDim), Viter(vDim), Vnew(vDim), Viternew(vDim), Uiter(Dim);
	int maxiter = 20;
	MatrixXd A(vDim, vDim), Jacob(vDim,vDim);
	double norm;
	double mat[vDim*vDim];
	double * mat_res = new double[vDim*vDim];

	convertUToV(U, V);
	Viter = V;
	Uiter = U;
	convertVToU(Uiter, Viter);

	int count_max = 10;
	int count_min = 3;
	int count_excess = 0;
	int count_decr = 0;


	while (cur_time < time) {
		count = 0;
		err = 1;
		status = true;
		while (err > 1.e-6 && count < maxiter)
		{
			count++;
			calc_matrix(A, Viter, Uiter, dt_ode);
	
			for (int i = 0; i < vDim; i++)
				for (int j = 0; j < vDim; j++)
					mat[i + j * vDim] = A(i, j);
	

			mat_res = r8mat_expm1(vDim, mat);

			for (int i = 0; i < vDim; i++)
			{
				Vnew(i) = 0;
				for (int j = 0; j < vDim; j++) 
					Vnew(i) += mat_res[i + j * vDim] * V(j);	
				
			}

			norm = calc_Jacob_num(Jacob, Viter, Uiter, V, dt_ode, vDim);
		
//			std::cout << "norm J = " << norm << std::endl;

			Viternew = Viter - Jacob.inverse()*(Viter - 0.5*Vnew - 0.5*V);
		    status = CheckV(Viternew, Uiter, vDim);
			err = calc_err(Viter, Viternew);
			Viter = Viternew;
			convertVToU(Uiter, Viter);

		}

		if (count >= maxiter) status = false;
		if (status)
		{
			if (count > count_max) count_excess++;
			else if (count < count_min)	count_decr++;

			V = Vnew;
			Viter = V;
			convertVToU(Uiter, Viter);
			cur_time = cur_time + dt_ode;

			if (count_excess >= 1)
			{
				dt_ode = 0.5 * dt_ode;
				count_excess = 0;
				count_decr = 0;
			}
			if (count_decr >= 3)
			{
				dt_ode = 2 * dt_ode;
				count_excess = 0;
				count_decr = 0;
			}

			if (cur_time + dt_ode > time) dt_ode = time - cur_time;
		}
		else
		{
			dt_ode = 0.5 * dt_ode;
			Viter = V;
			convertVToU(Uiter, Viter);
		}

	}
	U = Uiter;
	delete mat_res;
	
}*/

void calc_sorces_analytical_split(VectorXd& U, double dt, int n) // method 2
{
	calc_analitical_velocity_relax(U, dt);
	calc_analitical_pressure_relax(U, dt, n);
}


void calc_analitical_velocity_relax(VectorXd& U, double dt)
{
	ftype u1old = U(1) / U(0);
	ftype u2old = U(5) / U(4);
	ftype lambdav = -lambda * (U(0) + U(4)) / (U(4) * U(0));
	ftype u1 = (u1old * U(0) + u2old * U(4)) / (U(0) + U(4)) + U(4) * (u1old - u2old) / (U(0) + U(4)) * exp(lambdav * dt);
	ftype u2 = (u1old * U(0) + u2old * U(4)) / (U(0) + U(4)) - U(0) * (u1old - u2old) / (U(0) + U(4)) * exp(lambdav * dt);
	
	ftype p2old = pressure(U, 1);
	ftype p2 = lambda * (u1old - u2old) * (u1old - u2old) * (mygamma[1] - 1.0) / (2.0 * (1.0 - U(8)) * lambdav) * (exp(2 * lambdav * dt) - 1.0) + pressure(U, 1);
	ftype p1 = pressure(U, 0);
	
	U(1) = U(0) * u1;
	U(5) = U(4) * u2;
	
	U(3) = U(0) * (energy(p1, U(0) / U(8), 0) + 0.5 * u1 * u1);
	U(7) = U(4) * (energy(p2, U(4) / (1.0 - U(8)), 1) + 0.5 * u2 * u2);
	
}

void calc_analitical_pressure_relax(VectorXd& U, double dt, int n)
{
	ftype err = 1;
	ftype dt_ode = dt;
	int count = 0; 
	int maxiter = 50;
	
	ftype kp1, kp2;
	VectorXd V(3), Viter(3), Vnew(3), Viternew(3);
	
	// convert u->v
	V(0) = pressure(U, 0);
	V(1) = pressure(U, 1);
	V(2) = U(8);
	
	Viter = V;
	
	while (err > 1.e-12 && count < maxiter)
	{
		//count++;
		
		kp1 = -nu/ Viter(2) * (Viter(0) - Viter(1) + mygamma[0]*(Pref[0] + Viter(1)));
		kp2 =  nu * mygamma[1]/ (1.0 - Viter(2)) * (Viter(1) + Pref[1]);
		
		Vnew(0) = (-kp2 * V(0) + exp((kp1 - kp2) * dt_ode) * kp1 * (V(0) - V(1)) + kp1 * V(1)) / (kp1 - kp2);
		Vnew(1) = (-kp2 * V(0) + exp((kp1 - kp2) * dt_ode) * kp2 * (V(0) - V(1)) + kp1 * V(1)) / (kp1 - kp2);
		Vnew(2) = 1.0 - (1.0 - V(2)) * pow((V(1) + Pref[1]) / (Vnew(1) + Pref[1]), 1.0 / mygamma[1]);
		Viternew = 0.5 * (V + Vnew);
		
		err = calc_err(Viter, Viternew);
		Viter = Viternew;
	}
	//convert v->u
	U(8) = Vnew(2);
	ftype u1 = U(1) / U(0);
	ftype u2 = U(5) / U(4);
	U(3) = U(0) * (energy(Vnew(0), U(0) / U(8), 0) + 0.5 * u1 * u1);
	U(7) = U(4) * (energy(Vnew(1), U(4) / (1.0 - U(8)), 1) + 0.5 * u2 * u2);
}



/*
void calc_sorces_analytical(VectorXd& U, double dt, double& dt_new, int n, int &it) //method 3
{
	ftype dt_ode = dt;
	ftype time = dt;
	ftype cur_time = 0;
	int count;
	ftype err;
	bool status;
	VectorXd V(5), Viter(5), Vnew(5), Viternew(5), Uiter(Dim);
	VectorXd Cold(5), C(5), Cnew(5);
	MatrixXd A(5, 5), Aold(5,5), Anew(5,5), J(3,3);
	ftype rho1, rho2, dedr1, dedp1Inv, dedr2, dedp2Inv;
	ftype lambdav;
	ftype kp1, kp2;
	ftype un1, un2, p1_pr, p2_pr, p1_h, p2_h, D1, D2, C1, C2;
	ftype af, bf;
	int ii;

	int maxiter = 50;
	int count_max = 10;
	int count_min = 3;
	int count_excess = 0;
	int count_decr = 0;
	it = 0;

	convertUToV(U, V);
	Viter = V;
	Uiter = U;

	ftype a1rho1 = U(0);
	ftype a2rho2 = U(4);
	ftype rho1_new, rho2_new;
	lambdav = -lambda * (a1rho1 + a2rho2) / (a2rho2 * a1rho1);
	ftype normM;
	while (cur_time < time) {
		count = 0;
		err = 1;
		status = true;
		while (err > 1.e-8 && count < maxiter)
		{
			count++;

			Vnew(0) = (V(0) * a1rho1 + V(1) * a2rho2) / (a1rho1 + a2rho2) + a2rho2 * (V(0) - V(1)) / (a1rho1 + a2rho2) * exp(lambdav * dt_ode);
	        Vnew(1) = (V(0) * a1rho1 + V(1) * a2rho2) / (a1rho1 + a2rho2) - a1rho1 * (V(0) - V(1)) / (a1rho1 + a2rho2) * exp(lambdav * dt_ode);

			if (Vnew(0) < 1.e-20) Vnew(0) = 0;
			if (Vnew(1) < 1.e-20) Vnew(1) = 0;
 
			rho1 = a1rho1 / Viter(4);
			rho2 = a2rho2 / (1.0 - Viter(4));
			dedr1 = dedrho(Uiter, 0);
			dedp1Inv = 1.0 / dedp(Uiter, 0);
			dedr2 = dedrho(Uiter, 1);
			dedp2Inv = 1.0 / dedp(Uiter, 1);

			kp1 = nu * (dedr1 * rho1 / Viter(4) - Pi(Uiter) / a1rho1) * dedp1Inv;
			kp2 = nu * (-dedr2 * rho2 / (1.0 - Viter(4)) + Pi(Uiter) / a2rho2) * dedp2Inv;
	
			p1_h = (-kp2 * V(2) + exp((kp1 - kp2) * dt_ode) * kp1 * (V(2) - V(3)) + kp1 * V(3)) / (kp1 - kp2);
			p2_h = (-kp2 * V(2) + exp((kp1 - kp2) * dt_ode) * kp2 * (V(2) - V(3)) + kp1 * V(3)) / (kp1 - kp2);

			D1 = (Viter(0) - Ui(Uiter)) / a1rho1 * dedp1Inv;
			D2 = (Ui(Uiter) - Viter(1)) / a2rho2 * dedp2Inv;

			C2 = (lambda * (V(0) - V(1)) * D2 + kp2 / (lambdav - kp1) * lambda * (V(0) - V(1)) * D1)/(lambdav +
				kp2 + kp1*kp2/(lambdav - kp1));
			C1 = (-kp1 * C2 + lambda * (V(0) - V(1)) * D1) / (lambdav - kp1);
			
			p1_pr = C1 * exp(lambdav * dt_ode);
			p2_pr = C2 * exp(lambdav * dt_ode);

			Vnew(2) = p1_pr + p1_h;
			Vnew(3) = p2_pr + p2_h;
			Vnew(4) = 1.0 - (1.0 - V(4)) * pow((V(3) + Pref[1]) / (Vnew(3) + Pref[1]), 1.0 / mygamma[1]);
			Viternew = 0.5 * (V + Vnew);

			err = calc_err(Viter, Viternew, 5, ii);
			Viter = Viternew;
			convertVToU(Uiter, Viter);

		}
		
	
		if (status)
		{
			if (count > count_max) count_excess++;
			else if (count < count_min)	count_decr++;

			cur_time = cur_time + dt_ode;
			V = Vnew;
			Viter = V;
			convertVToU(Uiter, Viter);
			it++;
			if (count_excess >= 3)
			{
				dt_ode = 0.5 * dt_ode;
				count_excess = 0;
				count_decr = 0;
			}
			if (count_decr >= 3)
			{
				dt_ode = 2 * dt_ode;
				count_excess = 0;
				count_decr = 0;
			}
			if (cur_time + dt_ode > time) dt_ode = time - cur_time;
 		}
		else
		{
			dt_ode = 0.5 * dt_ode;
			Viter = V;
			convertVToU(Uiter, Viter);
		}
	}

	convertVToU(U, Vnew);

}


void calc_sorces_boost_runge_kutta_dopri5(VectorXd& U, double dt, int n)
{
	ftype time = dt;
	ftype dt_ode = 0.5*dt;
	ftype cur_time = 0;
	vector_type x2(5, 0.0);
	convertUToX(U, x2);
	struct stiff_system BaerNunziato;

	BaerNunziato.a1rho1 = U(0);
	BaerNunziato.a2rho2 = U(4);
 

	size_t num_of_steps2 = integrate_const(make_dense_output< runge_kutta_dopri5< vector_type > >(1.0e-6, 1.0e-6),
		BaerNunziato, x2, 0.0, time, dt_ode);


	convertXToU(U, x2);

}

void calc_sorces_boost_rosenbrock4(VectorXd& U, double dt, int n)
{
	ftype time = dt;
	ftype dt_ode = 0.5*dt;
	ftype cur_time = 0;
	vector_type x2(5, 0.0);
	convertUToX(U, x2);

	ftype Errv = fabs(x2[0] - x2[1]) / sqrt(x2[0] * x2[0] + x2[1] * x2[1]);
	ftype Errp = fabs(x2[2] - x2[3]) / sqrt(x2[2] * x2[2] + x2[3] * x2[3]);
	if (Errv < 1.e-7 && Errp < 1.e-7) return;

	struct stiff_system BaerNunziato;
	struct stiff_system_jacobi BaerNunziatoJacob;
 

	BaerNunziato.a1rho1 = U(0);
	BaerNunziato.a2rho2 = U(4);

	BaerNunziatoJacob.a1rho1 = U(0);
	BaerNunziatoJacob.a2rho2 = U(4);
	
	
	bool status = true;
		size_t num_of_steps = integrate_const(make_dense_output< rosenbrock4< double > >(1e-4, 1e-4),
			make_pair(BaerNunziato, BaerNunziatoJacob),
			x2, 0.0, time, dt_ode);

		if (x2[4] < 1.e-6)
		{
			x2[4] = 1.e-6;
		}
		else if (x2[4] > 1.0 - 1.e-6)
		{
			x2[4] = calc_max_equi_phi(U);
		}	
	
	convertXToU(U, x2);
	

}


void calc_vel_equi(VectorXd& U)
{
	ftype u1old = U(1) / U(0);
	ftype u2old = U(5) / U(4);

	ftype e1old = U(3) / U(0) - 0.5 * u1old * u1old;
	ftype e2old = U(7) / U(4) - 0.5 * u2old * u2old;
	ftype ui = Ui(U);

	ftype u = (U(0) * u1old + U(4) * u2old) / (U(0) + U(4));
	ftype e1 = e1old + 0.5 * (u - u1old) * (ui - u1old);
	ftype e2 = e2old + 0.5 * (u - u2old) * (ui - u2old);

	U(1) = U(0) * u;
	U(5) = U(4) * u;
	U(3) = U(0) * (e1 + 0.5 * u * u);
	U(7) = U(4) * (e2 + 0.5 * u * u);
}

void calc_press_equi(VectorXd& U)
{
	ftype err = 1;
	ftype a1old = U(8);
	ftype a1 = U(8);
	ftype dfda1, e1, e2;
	ftype f, p1, p2, dens1, dens2, pi, piaver;

	ftype u1old = U(1) / U(0);
	ftype u2old = U(5) / U(4);

	ftype up, vp, dupda1, dvpda1;

	ftype e1old = U(3) / U(0) - 0.5 * u1old * u1old;
	ftype e2old = U(7) / U(4) - 0.5 * u2old * u2old;
	ftype p1old = pressure(U, 0);
	ftype p2old = pressure(U, 1);
	ftype dp1da1, dp2da1, a1new, de1da1;
	ftype ddens1da1, ddens2da1;
	int count = 0;
	while (err > 1.e-10 && count < 500)
	{
		count++;
		dens1 = U(0) / a1;
		ddens1da1 = - U(0) / (a1 * a1);

		dens2 = U(4) / (1.0 - a1);
		ddens2da1 = U(4) / ((1.0 - a1) * (1.0 - a1));

		up = (mygamma[1] - 1.0) * dens2 *( e2old + 0.5 / U(4) * p2old * (a1 - a1old)) - mygamma[1] * Pref[1];
		vp = (1.0 - (mygamma[1] - 1) / U(4) * dens2 * 0.5 * (a1 - a1old));

		p2 = up / vp;

		dupda1 = (mygamma[1] - 1.0) * (e2old * ddens2da1 + 0.5 / U(4) * p2old * (dens2 + (a1 - a1old) * ddens2da1));
		dvpda1 = -(mygamma[1] - 1.0) / U(4) * 0.5 * (dens2 + (a1 - a1old) * ddens2da1);

		dp2da1 = (dupda1 * vp - dvpda1 * up) / (vp * vp);

		e1 = e1old - 0.5 * (p2old + p2) / U(0) * (a1 - a1old);
		e2 = e2old + 0.5 * (p2old + p2) / U(4) * (a1 - a1old);

		de1da1 = -0.5 / U(0) * ((p2old + p2) + (a1 - a1old) * dp2da1);
		p1 = (mygamma[0] - 1.0) * dens1 * e1 - mygamma[0] * Pref[0];
		dp1da1 = (mygamma[0] - 1.0) * (ddens1da1 * e1 + dens1 * de1da1);

		f = p1 - p2;
		dfda1 = dp1da1 - dp2da1;

		a1new = a1 - f / dfda1;

		if (a1new > (1.0 + 0.5 * (mygamma[1] - 1.0) * a1) / (1.0 + 0.5 * (mygamma[1] - 1.0)) - 1.e-10)
		{
			a1new = (1.0 + 0.5 * (mygamma[1] - 1.0) * a1) / (1.0 + 0.5 * (mygamma[1] - 1.0)) - 1.e-10;
		}


		if (a1new > 1.0 - 1.e-12) a1new = 1.0 - 1.e-12;
		if (a1new < 1.e-12) a1new = 1e-12;
		err = fabs(p1 - p2) / sqrt(p1 * p1 + p2 * p2);
		a1 = a1new;
	}

	if (count > 500) std::cout << "diverge!" << std::endl;

	e1 = e1old - 0.5 * (p2old + p2) / U(0) * (a1 - a1old);
	e2 = e2old + 0.5 * (p2old + p2) / U(4) * (a1 - a1old);

	U(8) = a1;
	U(3) = U(0) * (e1 + 0.5 * u1old * u1old);
	U(7) = U(4) * (e2 + 0.5 * u2old * u2old);

}

ftype calc_max_equi_phi(VectorXd& U)
{
	ftype err = 1;
	ftype a1old = U(8);
	ftype a1 = U(8);
	ftype dfda1, e1, e2;
	ftype f, p1, p2, dens1, dens2, pi, piaver;

	ftype u1old = U(1) / U(0);
	ftype u2old = U(5) / U(4);

	ftype up, vp, dupda1, dvpda1;

	ftype e1old = U(3) / U(0) - 0.5 * u1old * u1old;
	ftype e2old = U(7) / U(4) - 0.5 * u2old * u2old;
	ftype p1old = pressure(U, 0);
	ftype p2old = pressure(U, 1);
	ftype dp1da1, dp2da1, a1new, de1da1;
	ftype ddens1da1, ddens2da1;
	int count = 0;
	while (err > 1.e-10 && count < 500)
	{
		count++;
		dens1 = U(0) / a1;
		ddens1da1 = -U(0) / (a1 * a1);

		dens2 = U(4) / (1.0 - a1);
		ddens2da1 = U(4) / ((1.0 - a1) * (1.0 - a1));

		up = (mygamma[1] - 1.0) * dens2 * (e2old + 0.5 / U(4) * p2old * (a1 - a1old)) - mygamma[1] * Pref[1];
		vp = (1.0 - (mygamma[1] - 1) / U(4) * dens2 * 0.5 * (a1 - a1old));

		p2 = up / vp;

		dupda1 = (mygamma[1] - 1.0) * (e2old * ddens2da1 + 0.5 / U(4) * p2old * (dens2 + (a1 - a1old) * ddens2da1));
		dvpda1 = -(mygamma[1] - 1.0) / U(4) * 0.5 * (dens2 + (a1 - a1old) * ddens2da1);

		dp2da1 = (dupda1 * vp - dvpda1 * up) / (vp * vp);

		e1 = e1old - 0.5 * (p2old + p2) / U(0) * (a1 - a1old);
		e2 = e2old + 0.5 * (p2old + p2) / U(4) * (a1 - a1old);

		de1da1 = -0.5 / U(0) * ((p2old + p2) + (a1 - a1old) * dp2da1);
		p1 = (mygamma[0] - 1.0) * dens1 * e1 - mygamma[0] * Pref[0];
		dp1da1 = (mygamma[0] - 1.0) * (ddens1da1 * e1 + dens1 * de1da1);

		f = p1 - p2;
		dfda1 = dp1da1 - dp2da1;

		a1new = a1 - f / dfda1;

		if (a1new > (1.0 + 0.5 * (mygamma[1] - 1.0) * a1) / (1.0 + 0.5 * (mygamma[1] - 1.0)) - 1.e-10)
		{
			a1new = (1.0 + 0.5 * (mygamma[1] - 1.0) * a1) / (1.0 + 0.5 * (mygamma[1] - 1.0)) - 1.e-10;
		}


		if (a1new > 1.0 - 1.e-12) a1new = 1.0 - 1.e-12;
		if (a1new < 1.e-12) a1new = 1e-12;
		err = fabs(p1 - p2) / sqrt(p1 * p1 + p2 * p2);
		a1 = a1new;
	}

	if (count > 500) std::cout << "diverge!" << std::endl;

	return a1;
}

*/
