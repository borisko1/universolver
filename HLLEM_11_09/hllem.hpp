#include <iostream>
#include <Eigen/Dense>
// #define BOOST_UBLAS_TYPE_CHECK 0
// #include <boost/numeric/odeint.hpp>
#pragma once

using namespace Eigen;

typedef double ftype;



#include <utility>

// #include <boost/numeric/odeint.hpp>
// 
// #include <boost/phoenix/core.hpp>
// 
// #include <boost/phoenix/core.hpp>
// #include <boost/phoenix/operator.hpp>
#include <time.h> 
#include <fstream> 


using namespace std;
// using namespace boost::numeric::odeint;
// namespace phoenix = boost::phoenix;
// 
// //[ stiff_system_definition
// typedef boost::numeric::ublas::vector< double > vector_type;
// typedef boost::numeric::ublas::matrix< double > matrix_type;

/** Одномерная газовая динамика -- d=3, двухфазная модель Баера-Нунциато -- d=9 */
const int Dim=9;
const int vDim = 5;
const ftype mygamma[2] = {4.4, 1.4}; // water - air 
const ftype Pref[2] = { 6e8, 0.0}; // water - air


const ftype lambda = 1e9; // coefficients of relaxation
const ftype nu = 100;



#define USEHLLEM // comment this for HLL


inline ftype min(ftype a, ftype b) {return a<b?a:b;}
inline ftype max(ftype a, ftype b) {return a>b?a:b;}


/** Скорости распространения волн (собственные значения) */
ftype minmaxeigenvalue(VectorXd &Q, int mx);


void S_LR_calc(VectorXd&  QL  , VectorXd&  QR  , ftype & sL, ftype & sR);

/** Консервативные члены */
void flux (VectorXd&  Q  ,  VectorXd& F );
/** Уравнение сотояния (расчет давления) */
ftype pressure(VectorXd Q, int phase);
ftype dedrho(VectorXd& Q, int phase);
ftype d2edrhodp(VectorXd& Q, int phase);
ftype dedp(VectorXd& Q, int phase);
ftype energy(ftype pres, ftype dens, int phase);
ftype Ui(VectorXd& Q);
ftype dUidu1(VectorXd& Q);
ftype dUidu2(VectorXd& Q);
ftype Pi(VectorXd& Q);
ftype dPidp1(VectorXd& Q);
ftype dPidp2(VectorXd& Q);
ftype sound_speed(VectorXd &Q, int phase);


ftype calc_err(VectorXd& U1, VectorXd& U2);
ftype calc_err(VectorXd& U1, VectorXd& U2, int size, int& i);

/** Матрица A(Q) -- задается в зависимости от решаемой задачи 
 * v *oid calc_matrix_A (VectorXd Q, MatrixXd& A) {
 * 
 * MatrixXd A = MatrixXd::Zero(Dim, Dim), B = MatrixXd::Zero(Dim, Dim);
 * calc_matrix_B (Q, B);
 * 
 * A+=B;
 * }
 */

/** Матрица B(Q) -- задается в зависимости от решаемой задачи. */
void calc_matrix_B (VectorXd &Q, MatrixXd& B);

/** Интегральное среднее B по пути (ломаная из двух прямых отрезков). Используем трехточечную Гауссову квадратуру. */
void calc_B_mean (VectorXd &Qa, VectorXd &Qb, MatrixXd& Bmean);

void calc_D (VectorXd& QL, VectorXd& QR, VectorXd& D, int plus, ftype , int& );

void scheme_step(VectorXd& Unew, VectorXd& UC, VectorXd& UL, VectorXd& UR, ftype tau_h, ftype dt, ftype h,  int&, ftype *AlphaR, ftype *AlphaL, int n);

void scheme_source_step(VectorXd& UC, ftype dt, int n, int &it); 

void write_data(double time, ftype* X, VectorXd* U, int N, ftype* AlphaR , ftype* AlphaL);

bool Check(VectorXd& U, int& error); //checking for conservative variables

// /*
// struct stiff_system
// {
// 	ftype a1rho1;
// 	ftype a2rho2;
// 	void operator()(const vector_type& x, vector_type& dxdt, double /* t */)
// 	{
// 
// 		double ui = x[0];
// 		double pi = x[3];
// 		double eps = 1.e-12;
// 		double phi1 = x[4];
// 		double phi2 = 1.0 - x[4];
// 	
// 
// 		double de1dp1Inv = (mygamma[0] - 1.0) * a1rho1 / phi1;
// 		double de2dp2Inv = (mygamma[1] - 1.0) * a2rho2 / phi2;
// 		double kp1 = -nu * ((x[2] + mygamma[0] * Pref[0]) / (mygamma[0] - 1.0) + pi) * (mygamma[0] - 1.0) / phi1;
// 		double  kp2 = nu * ((x[3] + mygamma[1] * Pref[1]) / (mygamma[1] - 1.0) + pi) * (mygamma[1] - 1.0) / phi2;
// 
// 
// 		dxdt[0] = lambda / a1rho1 * (x[1] - x[0]);
// 		dxdt[1] = lambda / a2rho2 * (x[0] - x[1]);
// 		dxdt[2] = kp1* (x[2] - x[3]);
// 		dxdt[3] = lambda / a2rho2 * (ui - x[1]) * de2dp2Inv * (x[0] - x[1])
// 			+ kp2 * (x[2] - x[3]);
// 		dxdt[4] = nu * (x[2] - x[3]);
// 	}
// };

// /*
// struct stiff_system_jacobi
// {
// 	ftype a1rho1;
// 	ftype a2rho2;
// 	void operator()(const vector_type& x, matrix_type& J, const double& /* t */, vector_type& dfdt)
// 	{
// 
// 		double ui = x[0];
// 		double pi = x[3];
// 		double eps = 1.e-12;
// 		double phi1 = x[4];
// 		double phi2 = 1.0 - x[4];
// 
// 		double de1dp1Inv = (mygamma[0] - 1.0) * a1rho1 / phi1;
// 		double de2dp2Inv = (mygamma[1] - 1.0) * a2rho2 / phi2;
// 
// 		double dkp1dp1 = -nu / phi1;
// 		double kp1 = -nu * ((x[2] + mygamma[0] * Pref[0]) / (mygamma[0] - 1.0) + pi) * (mygamma[0] - 1.0) / phi1;
// 		double dkp1dp2 = -nu * (mygamma[0] - 1) / phi1;
// 		double dkp1da1 = -kp1 / phi1;
// 
// 		double dku2du1 = lambda / a2rho2 * de2dp2Inv;
// 		double ku2 = lambda / a2rho2 * (ui - x[1]) * de2dp2Inv;
// 		double dku2du2 = -lambda / a2rho2 * de2dp2Inv;
// 
// 
// 		double  kp2 = nu * ((x[3] + mygamma[1] * Pref[1]) / (mygamma[1] - 1.0) + pi) * (mygamma[1] - 1.0) / phi2;
// 		double dkp2dp2 = nu * mygamma[1] / phi2;
// 
// 		double dku2da1 = lambda * (x[0] - x[1]) * (mygamma[1] - 1) / (phi2 * phi2);
// 		double dkp2da1 = kp2 / phi2;
// 
// 		J(0, 0) = -lambda / a1rho1;
// 		J(0, 1) = lambda / a1rho1;
// 		J(0, 2) = 0;
// 		J(0, 3) = 0;
// 		J(0, 4) = 0;
// 
// 		J(1, 0) = lambda / a2rho2;
// 		J(1, 1) = -lambda / a2rho2;
// 		J(1, 2) = 0;
// 		J(1, 3) = 0;
// 		J(1, 4) = 0;
// 
// 		J(2, 0) = 0.0;
// 		J(2, 1) = 0.0;
// 		J(2, 2) = dkp1dp1 * (x[2] - x[3]) + kp1;
// 		J(2, 3) = dkp1dp2 * (x[2] - x[3]) - kp1;
// 		J(2, 4) = dkp1da1* (x[2] - x[3]);
// 
// 
// 		J(3, 0) = dku2du1 * (x[0] - x[1]) + ku2;
// 		J(3, 1) = dku2du2 * (x[0] - x[1]) - ku2;
// 		J(3, 2) = kp2;
// 		J(3, 3) = dkp2dp2 * (x[2] - x[3]) - kp2;
// 		J(3, 4) = dku2da1* (x[0] - x[1]) + dkp2da1 * (x[2] - x[3]);
// 
// 
// 		J(4, 0) = 0;
// 		J(4, 1) = 0;
// 		J(4, 2) = nu;
// 		J(4, 3) = -nu;
// 		J(4, 4) = 0;
// 
// 
// 		dfdt[0] = 0.0;
// 		dfdt[1] = 0.0;
// 		dfdt[2] = 0.0;
// 		dfdt[3] = 0.0;
// 		dfdt[4] = 0.0;
// 	}
// };
// */

//void calc_sorces_matexp(VectorXd& U, double dt, int n); // method 1 
void calc_sorces_analytical_split(VectorXd& U, double dt, int n); //method 2 
void calc_sorces_analytical(VectorXd& U, double dt, double& dt_new, int n, int &it); // method 3
void calc_sorces_boost_runge_kutta_dopri5(VectorXd& U, double dt, int n); // relaxation with boost
void calc_sorces_boost_rosenbrock4(VectorXd& U, double dt, int n); // relaxation with boost 
void calc_vel_equi(VectorXd& U); // equilibrium velocity relaxation 
void calc_press_equi(VectorXd& U); // equilibrium pressure relaxation
ftype calc_max_equi_phi(VectorXd& U); // equilibrium volume fraction
void calc_analitical_velocity_relax(VectorXd& U, double dt);
void calc_analitical_pressure_relax(VectorXd& U, double dt, int n);

void convertUToV(VectorXd& U, VectorXd& V); // convert conservative variables into primitive variables
void convertVToU(VectorXd& U, VectorXd& V); // convert primitive variables into conservative variables
//void convertUToX(VectorXd& U, vector_type& V); // convert conservative variables into primitive variables (boost arrays)
bool CheckV(VectorXd& V, VectorXd& U); //checking for primitive variables
void calc_matrix(MatrixXd& A, VectorXd& V, VectorXd& U, double dt_ode);
ftype calc_Jacob(MatrixXd& A, VectorXd& U, VectorXd& Uold, ftype dt);


inline ftype frac_bound (ftype value) {
	if (value > 1) {
		//fprintf(stderr,"fraction > 1!\n");
		return 1;
	}
	if (value < 0) {
		//fprintf(stderr,"fraction < 0!\n");
		return 0;
	}
	return value;
};

inline ftype frac(VectorXd Q, int phase) {
	/// хорошо бы сюда добавить проверочных костылей || добавил
	if (phase==0)
		return frac_bound( Q(8) );
	return frac_bound ( 1-Q(8) );
}

/** Вывод плотности фазы из консервативных переменных */
inline ftype dens (VectorXd Q, int phase) {
	int idens=4*phase;
	ftype d=Q(idens)/frac(Q,phase);
	if (d<0) return 0;
	return d;
}


inline ftype vel(VectorXd Q, int phase) {
	if (phase==0)
		return Q(1)/Q(0);
	return Q(5)/Q(4);
}

