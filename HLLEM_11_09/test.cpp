#include "hllem.hpp"

int main(int argc, char ** argv) {	
	const int N = 400;
	VectorXd * U = new VectorXd[N];
	VectorXd * Ucopy = new VectorXd[N];
	VectorXd*  Ucopy2 = new VectorXd[N];
	ftype* X = new ftype[N];
	ftype* AlphaR = new ftype[N];
	ftype* AlphaL = new ftype[N];
    int its[N];
	ftype L=1;
	std::cout.precision(16);
 //	omp_set_num_threads(1);

	
	for (int n=0; n<N; n++) {
		Ucopy[n] = VectorXd::Zero(Dim);
		Ucopy2[n] = VectorXd::Zero(Dim);
		U[n] = VectorXd::Zero(Dim);
        its[n]=0;
		X[n] = (L*(n+0.5))/N;
		AlphaR[n] = 1.0;
		AlphaL[n] = 1.0;
		ftype fi, dens[2], p[2], u[2];
		if (X[n] < 0.7) {
			// dens[0]=1; dens[1]=0.5; p[0]=1, p[1]=1; fi=0.4; // RP1
            // dens[0]=1; u[0]=0.9; p[0]=2.5; dens[1]=1  ; u[1]=0; p[1]=1; fi=0.9; // RP3
            //dens[0]=1; u[0]=0.0; p[0]=1.0; dens[1]=0.2; u[1]=0.0; p[1]=0.3; fi=0.8; // RP5
//           dens[0]=0.2068; u[0]=1.4166; p[0]=0.0416; dens[1]=0.5806; u[1]=1.5833; p[1]=1.375; fi=0.1; // RP6
			dens[0] = 1000; u[0] = 0.0; p[0] = 1.e9; dens[1] = 10; u[1] = 0; p[1] = 1.e9; fi = 1.0 - 1e-6; // LR2
		}
		else {
			// dens[0]=2; dens[1]=1.5; p[0]=2, p[1]=2; fi=0.8; // RP1
            // dens[0]=1; u[0]=0  ; p[0]=1  ; dens[1]=1.2; u[1]=1; p[1]=2; fi=0.2; // RP3
            // dens[0]=1; u[0]=0.0; p[0]=1.0; dens[1]=1.0; u[1]=0.0; p[1]=1.0; fi=0.3; // RP5
          //  dens[0]=2.2263; u[0]=0.9366; p[0]=6.0; dens[1]=0.4890; u[1]=-0.70138; p[1]=0.986; fi=0.2; // RP6
			dens[0] = 1000; u[0] = 0; p[0] =  1.e5; dens[1] = 10; u[1] = 0; p[1] = 1.e5; fi = 1.e-6; //  LR2
		}
		
		U[n]<<fi*dens[0],fi*dens[0]*u[0],0,fi* dens[0]*(energy(p[0],dens[0],0)+0.5*u[0]*u[0]),(1-fi)*dens[1],(1-fi)*dens[1]*u[1],0,(1-fi)*dens[1]*(energy(p[1], dens[1], 1) +0.5*u[1]*u[1]),fi;
		Ucopy2[n] = U[n];
		Ucopy[n] = U[n];
	}
	ftype t=0, Tmax = argc>1 ? atof(argv[1])  : 220.e-6;//0.1 for all except RP5
	ftype CFL=0.9;
	ftype dt, h = L / N;
	ftype dt_new = 1;

	write_data(0.0, X, U, N, AlphaR, AlphaL);
	
	ftype Lam=0;
	int it=0;
	int wcount = 0;
	while (t<Tmax) {
		Lam=0;
		for (int n=0; n<N; n++) {
			ftype Smax = max(fabs(minmaxeigenvalue(U[n],0)), fabs(minmaxeigenvalue(U[n],1)));
			if (Lam<Smax) Lam=Smax;
		}
		ftype tau_h=CFL/Lam;
		dt = tau_h * h;
		if (it == 0) dt = min(dt, 1e-11);

		std::cerr << "dt0 = " << dt << std::endl;
		if (t + dt > Tmax) dt = Tmax - t;

		
		int time1 = clock();
			
#pragma omp parallel for
			for (int n = 1; n < N - 1; n++)
				scheme_step(Ucopy[n], U[n], U[n - 1], U[n + 1], tau_h, dt, h, its[n], AlphaR, AlphaL, n);



			time1 = clock() - time1;
			std::cerr << "scheme_step takes  " << ((float)time1)/CLOCKS_PER_SEC<< std::endl;

			int time2 = clock();
#pragma omp parallel for
			for (int n = 1;n < N - 1;n++)
			{
				scheme_source_step(Ucopy[n], dt, n, it);
			}
			for (int n = 1; n < N - 1; n++) U[n] = Ucopy[n];
			// b.c.
			U[0]=U[1]; U[N-1]=U[N-2];
			
			time2 = clock() - time2;
			std::cerr << "scheme_source_step takes  " << ((float)time2)/CLOCKS_PER_SEC << std::endl;


		t+=dt;
		it++;
		std::cerr << "t = " << t << std::endl;
		if (t > wcount*Tmax*0.01)
		{
			write_data(t, X, U, N, AlphaR, AlphaL);
			wcount++;
		}

	}

	//write_data(t, X, U, N, AlphaR, AlphaL);
	for (int n=0; n<N; n++){
		std::cout << (L*(n+0.5))/N << " " << pressure(U[n],0)<< std::endl;
	}
	std::cout << "\n" << std::endl;
	for (int n=0; n<N; n++){
		std::cout << (L*(n+0.5))/N << " " << pressure(U[n],1)<< std::endl;
	}
	std::cout << "\n" << std::endl;
    for (int n=0; n<N; n++){
		std::cout << (L*(n+0.5))/N << " " << U[n](0)/U[n](8) << std::endl;
	}
	std::cout << "\n" << std::endl;
    for (int n=0; n<N; n++){
		std::cout << (L*(n+0.5))/N << " " << U[n](4)/(1-U[n](8)) << std::endl;
	}
	std::cout << "\n" << std::endl;
	for (int n=0; n<N; n++){
		std::cout << (L*(n+0.5))/N << " " << U[n](8)<< std::endl;
	}
	std::cout << "\n" << std::endl;
	for (int n=0; n<N; n++){
		std::cout << (L*(n+0.5))/N << " " << its[n]<< std::endl;
	}
}


