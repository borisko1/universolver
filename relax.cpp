#include "hllem.hpp"

// ------------------------------Relaxation -------------------------------------

void source_step(VectorXd& UC, ftype dt, int n, int &it)
{
	calc_sorces_analytical_split(UC, dt, n); // method 2
	//calc_sorces_matexp(UC, dt, n); //method 1
	//calc_sorces_boost_rosenbrock4(UC, dt, n);
	//calc_sorces_boost_runge_kutta_dopri5(UC, dt, n);
	
	int error = 1;
	//if (!Check(UC, error)) std::cerr << "Check failed: scheme_source_step " << error << std::endl;
}


void calc_sorces_analytical_split(VectorXd& U, double dt, int n) // method 2
{
	if (MODEL_ID==7) calc_analitical_velocity_relax(U, dt);
	if (MODEL_ID>5) calc_analitical_pressure_relax(U, dt, n);
}


void calc_analitical_velocity_relax(VectorXd& U, double dt)
{
	ftype u1old = vel(U,0);
	ftype u2old = vel(U,1);
	ftype lambdav = -lambda * (U(0) + U(4)) / (U(4) * U(0));
	ftype u1 = (u1old * U(0) + u2old * U(4)) / (U(0) + U(4)) + U(4) * (u1old - u2old) / (U(0) + U(4)) * exp(lambdav * dt);
	ftype u2 = (u1old * U(0) + u2old * U(4)) / (U(0) + U(4)) - U(0) * (u1old - u2old) / (U(0) + U(4)) * exp(lambdav * dt);
	
	ftype p2old = pressure(U, 1);
	ftype p2 = lambda * (u1old - u2old) * (u1old - u2old) * (mygamma[1] - 1.0) / (2.0 * frac(U,1) * lambdav) * (exp(2 * lambdav * dt) - 1.0) + pressure(U, 1);
	ftype p1 = pressure(U, 0);
	
	U(1) = U(0) * u1;
	U(5) = U(4) * u2;
	
	U(2) = U(0)     * (e_p(p1, U(0) / frac(U,0), 0) + 0.5 * u1 * u1);
	U(Dim-1) = U(4) * (e_p(p2, U(4) / frac(U,1), 1) + 0.5 * u2 * u2);
	//////////////////////////////////////////////////////////////// old code
// 	ftype u1old = U(1) / U(0);
// 	ftype u2old = U(5) / U(4);
// 	ftype lambdav = -lambda * (U(0) + U(4)) / (U(4) * U(0));
// 	ftype u1 = (u1old * U(0) + u2old * U(4)) / (U(0) + U(4)) + U(4) * (u1old - u2old) / (U(0) + U(4)) * exp(lambdav * dt);
// 	ftype u2 = (u1old * U(0) + u2old * U(4)) / (U(0) + U(4)) - U(0) * (u1old - u2old) / (U(0) + U(4)) * exp(lambdav * dt);
// 	
// 	ftype p2old = pressure(U, 1);
// 	ftype p2 = lambda * (u1old - u2old) * (u1old - u2old) * (mygamma[1] - 1.0) / (2.0 * (1.0 - U(8)) * lambdav) * (exp(2 * lambdav * dt) - 1.0) + pressure(U, 1);
// 	ftype p1 = pressure(U, 0);
// 	
// 	U(1) = U(0) * u1;
// 	U(5) = U(4) * u2;
// 	
// 	U(3) = U(0) * (energy(p1, U(0) / U(8), 0) + 0.5 * u1 * u1);
// 	U(7) = U(4) * (energy(p2, U(4) / (1.0 - U(8)), 1) + 0.5 * u2 * u2);
	
}

void calc_analitical_pressure_relax(VectorXd& U, double dt, int n)
{
	ftype err = 1;
	ftype dt_ode = dt;
	int count = 0; 
	int maxiter = 50;
	
	ftype kp1, kp2;
	VectorXd V(3), Viter(3), Vnew(3), Viternew(3);
	
	// convert u->v
	V(0) = pressure(U, 0);
	//if (V(0) < 0) V(0)=0;
	V(1) = pressure(U, 1);
	//if (V(1) < 0) V(1)=0;
	V(2) = frac(U,0);
	
	ftype pcorr=0;
	for (int i=0; i<2; i++) pcorr+=fabs(V(i));
	int correct_p=0;
	if (V(0)<0 || V(1)<0) {
		correct_p=1;
		V(0)+=pcorr;
		V(1)+=pcorr;
	}
	
	Viter = V;
	Vnew = V;
	
	while (err > 1.e-12 && count < maxiter)
	{
		//count++;
		
		kp1 = -nu/ Viter(2) * (Viter(0) - Viter(1) + mygamma[0]*(Pref[0] + Viter(1)));
		kp2 =  nu * mygamma[1]/ (1.0 - Viter(2)) * (Viter(1) + Pref[1]);
		ftype dkp = kp1-kp2;
		if (dkp==0) dkp += 1e-10;
		
		Vnew(0) = (-kp2 * V(0) + exp((kp1 - kp2) * dt_ode) * kp1 * (V(0) - V(1)) + kp1 * V(1)) / dkp;
		Vnew(1) = (-kp2 * V(0) + exp((kp1 - kp2) * dt_ode) * kp2 * (V(0) - V(1)) + kp1 * V(1)) / dkp;
		Vnew(2) = 1.0 - (1.0 - V(2)) * pow((V(1) + Pref[1]) / (Vnew(1) + Pref[1]), 1.0 / mygamma[1]);
		Viternew = 0.5 * (V + Vnew);
		
		err = calc_err(Viter, Viternew);
		Viter = Viternew;
	}
	//convert v->u
	U(3) = Vnew(2);
	ftype pr1 = correct_p==0 ? Vnew(0) : Vnew(0) - pcorr;
	ftype pr2 = correct_p==0 ? Vnew(1) : Vnew(1) - pcorr;
	ftype u1 = vel(U,0), u2 = vel(U,1);
	U(2) = U(0) *     (e_p(pr1, dens(U,0), 0) + 0.5 * u1 * u1);
	U(Dim-1) = U(4) * (e_p(pr2, dens(U,1), 1) + 0.5 * u2 * u2);
	
/*	
	while (err > 1.e-12 && count < maxiter)
	{
		//count++;
		
		kp1 = -nu/ Viter(2) * (Viter(0) - Viter(1) + mygamma[0]*(Pref[0] + Viter(1)));
		kp2 =  nu * mygamma[1]/ (1.0 - Viter(2)) * (Viter(1) + Pref[1]);
		
		Vnew(0) = (-kp2 * V(0) + exp((kp1 - kp2) * dt_ode) * kp1 * (V(0) - V(1)) + kp1 * V(1)) / (kp1 - kp2);
		Vnew(1) = (-kp2 * V(0) + exp((kp1 - kp2) * dt_ode) * kp2 * (V(0) - V(1)) + kp1 * V(1)) / (kp1 - kp2);
		Vnew(2) = 1.0 - (1.0 - V(2)) * pow((V(1) + Pref[1]) / (Vnew(1) + Pref[1]), 1.0 / mygamma[1]);
		Viternew = 0.5 * (V + Vnew);
		
		err = calc_err(Viter, Viternew);
		Viter = Viternew;
	}
	//convert v->u
	U(8) = Vnew(2);
	ftype u1 = U(1) / U(0);
	ftype u2 = U(5) / U(4);
	U(3) = U(0) * (energy(Vnew(0), U(0) / U(8), 0) + 0.5 * u1 * u1);
	U(7) = U(4) * (energy(Vnew(1), U(4) / (1.0 - U(8)), 1) + 0.5 * u2 * u2);*/
}


