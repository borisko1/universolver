#include "hllem.hpp"
#include <fstream>

ftype calc_err(VectorXd& U1, VectorXd& U2)
{
	VectorXd temp = U1 - U2;
	return temp.norm() / (U1.norm() + U2.norm() + 1.e-12);
}



// checking for conservative variables
bool Check(VectorXd& U, int& error) 
{
	for (int i = 0; i < Dim; i++)
	{
		if (  std::isnan( U(i) ) )
		{
			//std::cerr << "U(i)=" << U(i) << std::endl;
			error = -999;
			return false;
		}
	}

	if (frac(U,0) > 1.0)
	{
		error = -8;
		return false;
	}
	else if (frac(U,0) < 0)
	{
		error = -9;
		return false;

	}
	else if (U(0) < 0)
	{
		error = 0;
		return false;
	}
	else if (U(4) < 0)
	{
		error = -4;
		return false;
	}
	else if (ene(U,0) < 0)
	{
		error = -3;
		return false;
	}
	else if (ene(U,1) < 0)
	{
		error = -7;
		return false;
	}
	else return true;
}


std::string myreplace(std::string& s, const std::string& toReplace, const std::string& replaceWith)
{
	return(s.replace(s.find(toReplace), toReplace.length(), replaceWith));
}

void write_data(double time, ftype* X, VectorXd* U, int N, ftype*  AlphaR, ftype*  AlphaL)
{
	std::string name = "Prop_";
	std::string time_name = std::to_string(time);
	myreplace(time_name, ".", "p");
	name += time_name;
	name += ".dat";
	std::fstream plot(name, std::ios::out);
	ftype v1, v2, c1, c2, cpow2, cw;
	plot << "X, Pres1, Pres2, Phi, Ux1, Ux2, dens1, dens2, e1, e2, c1, c2, c, cw, odetime, ar, al " << std::endl;
	for (int i = 0; i < N; i++) {
		v1 = vel( U[i],0);
		v2 = vel( U[i],1);
		c1 = sound_speed(U[i], 0);
		c2 = sound_speed(U[i], 1);
		cpow2 = U[i][0] * c1 * c1 + U[i][4] * c2 * c2;
		cpow2 = cpow2 / (U[i][0] + U[i][4]);
		cw = frac(U[i],0) / (U[i][0] / frac(U[i],0) * c1 * c1) + (1.0 - frac(U[i],0)) / (U[i][4] / (1.0 - frac(U[i],0)) * c2 * c2);
		cw = 1.0 / ((U[i][0] + U[i][4]) * cw);
		plot << X[i] << ", " << pressure(U[i], 0) << ", " << pressure(U[i], 1) << ", "
			<< frac(U[i],0) << ", " << v1 << ", " << v2 << ", " << U[i][0] / frac(U[i],0) << ", "
			<< U[i][4] / (1.0 - frac(U[i],0)) << ", " << U[i][3] / U[i][0] - 0.5 * v1 * v1 << ", "
			<< U[i][7] / U[i][4] - 0.5 * v2 * v2 << ", " << c1 << ", " << c2 << ", " << sqrt(cpow2) << ", " << sqrt(cw) << 
			", " << AlphaR[i] <<", " << AlphaL[i]<< std::endl;
	}
	
}

