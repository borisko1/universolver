set term pdf enhanced
set key top left
set xlabel "x"

do for [i=1:4] {
  infile = sprintf('RP%d.dat',i)
  outfile = sprintf('RP%d_s.pdf',i)
  set output outfile
  set ylabel '{/Symbol r}_s'
  plot infile i 2 w p ls 8 t "HLLEM"
  outfile = sprintf('RP%d_g.pdf',i)
  set output outfile
  set ylabel '{/Symbol r}_g'
  plot infile i 3 w p ls 8 t "HLLEM"
}
