#include "hllem.hpp"

int main(int argc, char ** argv) {
	auto timer_start = omp_get_wtime( );
	int its[N];
	
	numerical_method Model;
	Model.init();
	
	ftype L = 1;
	Model.L=L; Model.h=Model.L/N ;
	for (int n=0; n<N; n++) {
		its[n]=0;
		ftype x = (L*(n+0.5))/N;
		
		ftype fi, dens[2], p[2], vel[2]={0,0}, df = 1e-6;
		
		ftype x0=0.7;
		if (x<x0) {
			//dens[0]=1; dens[1]=dens[0]; vel[0]=1; vel[1]=vel[0]; p[0]=1, p[1]=p[0]; fi=0.4;
			//dens[0]=1; dens[1]=0.5; p[0]=1, p[1]=1; fi=0.4; // RP1
			//dens[0]=800.0; dens[1]=1.5; p[0]=500; p[1]=2; fi=0.4; // RP2 
			//dens[0]=1; vel[0]=0.9; p[0]=2.5; dens[1]=1  ; vel[1]=0; p[1]=1; fi=0.9; // RP3
			//dens[0]=1; vel[0]=0.0; p[0]=1.0; dens[1]=0.2; vel[1]=0.0; p[1]=0.3; fi=0.8; // RP5
			//dens[0]=0.2068; vel[0]=1.4166; p[0]=0.0416; dens[1]=0.5806; vel[1]=1.5833; p[1]=1.375; fi=0.1; // RP6
			dens[0]=1000; vel[0]=0; p[0]=1e9; dens[1]=10; vel[1]=vel[0]; p[1]=p[0]; fi=1-df; // Test 6
			
		}
		else {
			//dens[0]=1; dens[1]=dens[0]; vel[0]=1; vel[1]=vel[0]; p[0]=1, p[1]=p[0]; fi=0.6;		
			//dens[0]=2; dens[1]=1.5; p[0]=2, p[1]=2; fi=0.8; // RP1
			//dens[0]=1000.0; dens[1]=1.0; p[0]=600; p[1]=1; fi=0.3; // RP2 
			//dens[0]=1; vel[0]=0  ; p[0]=1  ; dens[1]=1.2; vel[1]=1; p[1]=2; fi=0.2; // RP3
			// dens[0]=1; vel[0]=0.0; p[0]=1.0; dens[1]=1.0; vel[1]=0.0; p[1]=1.0; fi=0.3; // RP5
			//dens[0]=2.2263; vel[0]=0.9366; p[0]=6.0; dens[1]=0.4890; vel[1]=-0.70138; p[1]=0.986; fi=0.2; // RP6
			dens[0]=1000; vel[0]=0; p[0]=1e5; dens[1]=10; vel[1]=vel[0]; p[1]=p[0]; fi=df; // Test 6
		}
		if (MODEL_ID == 7) {
			Model.U[n]<<fi*dens[0],fi*dens[0]*vel[0]    ,fi*dens[0]*(e_p(p[0],dens[0],0)+0.5*vel[0]*vel[0])    ,fi,
			(1-fi)*dens[1]  ,(1-fi)*dens[1]*vel[1],(1-fi)*dens[1]*(e_p(p[1],dens[1],1)+0.5*vel[1]*vel[1]);
		}
		if (MODEL_ID == 6) {
			Model.U[n]<<fi*dens[0], (fi*dens[0]+(1-fi)*dens[1])*vel[0]    ,fi*dens[0]*(e_p(p[0],dens[0],0)+0.5*vel[0]*vel[0])    ,fi,
			(1-fi)*dens[1]  ,(1-fi)*dens[1]*(e_p(p[1],dens[1],1)+0.5*vel[0]*vel[0]);
		}
		if (MODEL_ID == 5) {
			///
			ftype rmean=(fi*dens[0]+(1-fi)*dens[1]);
			Model.U[n]<<fi*dens[0],rmean*vel[0],rmean*(e_p(p[0],rmean,0,fi)+0.5*vel[0]*vel[0]),fi,(1-fi)*dens[1];
		}
		// U[n]<<dens[0],dens[0]*vel[0],p[0]/(mygamma[0]-1)+0.5*dens[0]*vel[0]*vel[0]; // ,fi*dens[0],fi;
		Model.W[n]=Model.U[n];
	}
	ftype Tmax = argc>1 ? atof(argv[1])  : 220e-6;
	Model.CFL=0.9;
	int it;
	
	while (Model.time<Tmax) {
		
// 		#pragma omp parallel for
// 		for (int n=1; n<N-1; n++) 
// 			scheme_step(W[n], U[n], U[n-1], U[n+1], tau_h, dt, h, its[n], AlphaR, AlphaL, n);
// 		
// 		#ifdef RELAX
// 		#pragma omp parallel for
// 		for (int n = 1;n < N - 1;n++)
// 		{
// 			scheme_source_step(W[n], dt, n, it);
// 		}
// 		#endif
// 		
// 		#pragma omp parallel for
// 		for (int n = 1; n < N - 1; n++) U[n] = W[n];
// 		U[0]=U[1]; U[N-1]=U[N-2]; /// bc
		//time2 = clock() - time2;
		//std::cout << "scheme_source_step takes  " << ((float)time2)/CLOCKS_PER_SEC << std::endl;
		ftype t = Model.time;
		fprintf(stderr,"Progress %g%%, t=%g\n", 100*t/Tmax, t);
		Model.global_time_step();
	}
	Model.out();
	Model.turn_off();
	auto sim_time = omp_get_wtime( ) - timer_start;
	std::cout << "\n# Overall time of simulation = " << sim_time << " seconds." << std::endl;
	
}
