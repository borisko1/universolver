#include "hllem.hpp"

void numerical_method::init () {
	U = new VectorXd  [N];
	W = new VectorXd  [N];
	slope = new VectorXd [N];
	for (int n=0; n<N; n++) {
		U[n] = VectorXd::Zero(Dim);
		W[n] = VectorXd::Zero(Dim);
		slope[n]= VectorXd::Zero(Dim);
		its[n]=0;
	}
	AlphaL= new ftype [N];
	AlphaR= new ftype [N];
	time=0;
}

void numerical_method::dQ (int n, VectorXd & result) {
	result = VectorXd::Zero(Dim);
	#ifndef SECONDORDERTVD
	return;
	#endif
	VectorXd UL(Dim), UR(Dim), UC(Dim);
	UC = U[n]  ;
	UL = U[n-1];
	UR = U[n+1];
// 	int error = 1;
// 	if (!Check(UL, error) || !Check(UC, error) || !Check(UR, error) ){
// 		result = VectorXd::Zero(Dim);
// 		return;
// 	}
	for (int i=0; i<Dim; i++)
		result(i)=minmod(UC(i)-UL(i),UR(i)-UC(i));
}

void numerical_method::calc (){
	ftype tau_h=tau/h;
	
	//ftype min_err_in_iterations=1e-20;
	ftype min_err_in_iterations = 1e-15;
	
	
	// calc slopes
	#pragma omp parallel for
	for (int n=1; n<N-1; n++) dQ(n,slope[n]);
	slope[0]=VectorXd::Zero(Dim); slope[N-1]=VectorXd::Zero(Dim);
	
	// half-time step
	#pragma omp parallel for
	for (int n=0; n<N; n++) {
		VectorXd UmL(Dim), UmR(Dim), UpL(Dim), UpR(Dim), Unew(Dim), 
		dQn(Dim), Qm(Dim), Qp(Dim), dtQ(Dim), FL(Dim), FR(Dim), Dmean(Dim), Dm(Dim), Dp(Dim);
		VectorXd Dmhllem(Dim), Dphllem(Dim);
		VectorXd QstarL(Dim), QstarR(Dim);
		MatrixXd B=MatrixXd::Zero(Dim, Dim);
		dQn=slope[n];
		calc_matrix_B (U[n],B);
		flux(U[n]-0.5*dQn,FL); flux(U[n]+0.5*dQn,FR);
		dtQ = -(FR-FL)/h - B * dQn/h;
		W[n]=U[n]+0.5*tau*dtQ;
	}
	
	// second half time step (with neighbours)
	#pragma omp parallel for
	for (int n=1; n<N-1; n++) {
		int it=0;
		VectorXd UmL(Dim), UmR(Dim), UpL(Dim), UpR(Dim), Unew(Dim), 
		dQn(Dim), Qm(Dim), Qp(Dim), dtQ(Dim), FL(Dim), FR(Dim), Dmean(Dim), Dm(Dim), Dp(Dim);
		VectorXd Dmhllem(Dim), Dphllem(Dim);
		VectorXd QstarL(Dim), QstarR(Dim);
		MatrixXd B=MatrixXd::Zero(Dim, Dim);
		
		UmL=W[n-1]+0.5*slope[n-1];
		UmR=W[n]  -0.5*slope[n];
		UpL=W[n]  +0.5*slope[n];
		UpR=W[n+1]-0.5*slope[n+1];
		calc_matrix_B (W[n],B);
		
		flux(UmR,FL); flux(UpL,FR);
		int itm=0, itp=0;
		calc_Dhll (UmL  , UmR  ,  Dm , 1, min_err_in_iterations, itm, QstarL, n);
		calc_Dhll (UpL  , UpR  ,  Dp , 0, min_err_in_iterations, itp, QstarR,  n);
		its[n]=1+(itm+itp)/2;
		Dmean = Dp+Dm;
		int error = 1;
		#ifdef USEHLLEM
		calc_Dhllem(UmL  , UmR  , Dmhllem, QstarL, 1, min_err_in_iterations, AlphaL, n);
		calc_Dhllem(UpL  , UpR  , Dphllem, QstarR, 0, min_err_in_iterations, AlphaR, n);
		ftype alpha_min = min(AlphaL[n], AlphaR[n]);
		Unew=U[n]-tau_h*(Dmean + alpha_min*(Dphllem + Dmhllem) + (FR-FL) + B * slope[n]); 
		
		if (!Check(Unew, error)) {
			//	std::cerr << "Check failed: scheme_step " << error <<  ", n = " << n << ", alpha_min = "<< alpha_min << std::endl;
			Unew=U[n]-tau_h*(Dmean + (FR-FL) + B * slope[n]); 
			//	if (!Check(Unew, error)) std::cerr << "Check failed: scheme_step for hll " << error << std::endl;
		}
		
		#else
		Unew=U[n]-tau_h*(Dmean + (FR-FL) + B * slope[n]); 
		// if (!Check(Unew, error)) std::cerr << "Check failed: scheme_step for hll " << error << ", n = " << n << std::endl;
		#endif
		#ifdef RELAX
		source_step(Unew, tau, n, it);
		#endif
		U[n]=Unew;
	}
}

void numerical_method::global_time_step(){
	ftype Lam=0;
	for (int n=0; n<N; n++) {
		ftype Smax = max(fabs(minmaxeigenvalue(U[n],0)), fabs(minmaxeigenvalue(U[n],1)));
		if (Lam<Smax) Lam=Smax;
	}
	tau = CFL/Lam*h;
	fprintf(stderr,"tau=%g at t=%g\n", tau, time);
	calc();
	U[0]=U[1]; U[N-1]=U[N-2];
	time += tau;
}
void numerical_method::turn_off(){
	delete [] U; 
	delete [] W;
	delete [] slope;
	delete [] AlphaL;
	delete [] AlphaR;
}
