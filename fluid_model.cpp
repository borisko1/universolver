#include "hllem.hpp"

const ftype small_number = 1e-10;
const ftype frac_min = small_number;

inline ftype frac_bound (ftype value) {
	if (value > 1-frac_min) {
		//fprintf(stderr,"fraction > 1!\n");
		return 1 - frac_min;
	}
	if (value < frac_min) {
		//fprintf(stderr,"fraction < 0!\n");
		return frac_min;
	}
	return value;
};

ftype frac(VectorXd Q, int phase) {
/// хорошо бы сюда добавить проверочных костылей || добавил
	if (phase==0)
		return frac_bound( Q(3) );
	return frac_bound ( 1-Q(3) );
}

/** Вывод плотности фазы из консервативных переменных */
ftype dens (VectorXd Q, int phase) {
	int idens=4*phase;
	ftype d=Q(idens)/frac(Q,phase);
	//if (d<0) return small_number;
	return d;
}


ftype vel(VectorXd Q, int phase) {
	if (MODEL_ID==7) {
		if (phase==0)
			return Q(1)/Q(0);
		return Q(5)/Q(4);
	}
	return Q(1)/(Q(0)+Q(4));
}

/** Вывод внутренней энергии фазы из консервативных переменных */
ftype ene (VectorXd Q, int phase) {
	if (MODEL_ID==7) {
		int shift = 4*phase;
		return Q(shift + 2)/Q(shift) - 0.5*Q(shift +1)*Q(shift +1)/(Q(shift)*Q(shift));
	}
	ftype rho = Q(0) + Q(4) , u = vel(Q);
	if (MODEL_ID==6) {
		int ie=(phase==0?2:5), idens=(phase==0?0:4);
		return Q(ie)/Q(idens)-0.5*u*u;
	}
	if (MODEL_ID==5) {
		return Q(2)/rho - 0.5*u*u;
	}
}


ftype e_p(ftype pres, ftype dens, int phase, ftype fr)
{
	if (MODEL_ID>5) 
		return (pres + mygamma[phase] * Pref[phase]) / ((mygamma[phase] - 1) * dens);
	return (pres*gmean(fr) + pimean(fr)) / dens;
}

ftype pimean(ftype fr) {
	return fr*mygamma[0]*Pref[0]/(mygamma[0]-1) + (1-fr)*mygamma[1]*Pref[1]/(mygamma[1]-1);
}

ftype gmean(ftype fr) {
	return fr/(mygamma[0]-1) + (1-fr)/(mygamma[1]-1);
}

/** Уравнение состояния: расчет давления фазы */
ftype pressure (VectorXd Q, int phase) {
	ftype pr;
	pr = dens(Q,phase)*ene(Q,phase)*(mygamma[phase]-1) - mygamma[phase]*Pref[phase];
	if (pr<0) {
		//pr = small_number;
		//fprintf(stderr,"Negative pressure!\n");
	}
	return pr;
}

ftype p5 (VectorXd Q) {
	ftype rho = Q(0) + Q(4);
	ftype pr = (rho*ene(Q) - pimean(frac(Q,0)))/gmean(frac(Q,0));
	if (pr<0) {
		pr = small_number;
		//fprintf(stderr,"Negative pressure!\n");
	}
	return pr;
}

/** Уравнение состояния: расчет частных производных внутренней энергии по давлению и плотности */
ftype de_dr(VectorXd Q, int phase) {
	return -ene(Q,phase) / dens(Q,phase);
}
ftype de_dp(VectorXd Q, int phase){
	return 1/(dens(Q,phase) * (mygamma[phase]-1) );
}


/** Уравнение состояния: расчет скорости звука фазы */
ftype a_a (VectorXd Q, int phase) {
	ftype c2 = //(pressure(Q,phase)/(dens(Q,phase)*dens(Q,phase)) - de_dr(Q,phase))/de_dp(Q,phase);
				 mygamma[phase] * (pressure(Q,phase) + Pref[phase])/dens(Q,phase);
	if (c2 < 0) {
		//fprintf(stderr,"Negative c^2!\n");
		c2=0;
	}
	return c2;
}
ftype sound_speed (VectorXd Q, int phase) {
	return sqrt(a_a(Q,phase));
}

ftype pI (VectorXd Q) {
	return pressure(Q,1);
}
ftype uI (VectorXd Q) {
	return Q(1)/Q(0);
}
ftype K (VectorXd Q) {
	//return 0;
	//ftype rcc1=dens(Q,0)*a_a(Q,0), rcc2=dens(Q,1)*a_a(Q,1);
	ftype rcc1 = mygamma[0]*(p5(Q) + Pref[0]), rcc2 = mygamma[1]*(p5(Q)+Pref[1]);
	
	return (rcc2-rcc1)/(rcc1/frac(Q,0) + rcc2/frac(Q,1));
}
/** Матрица B(Q) для неконсервативных членов */

void calc_matrix_B (VectorXd Q, MatrixXd& B) {
	B=MatrixXd::Zero(Dim, Dim); //return;
	if (MODEL_ID==7){
		ftype p_I=pI(Q), ui=uI(Q);
		B.col(3) << 0 , -p_I, -p_I*ui, ui, 0 , p_I, p_I*ui;
		//B.col(Dim-1) << 0 , 0, 0, 0, 0 , -pi , -pi*ui, ui;
		return;
	}
	ftype rmean=Q(0)+Q(4), u = vel(Q);
	if (MODEL_ID==6) {
		B.col(3) << 0 , 0, 0, u, 0 , 0;
		return;
	}
	if (MODEL_ID==5) {
		//K=0;
		B.row(3) << K(Q)*u/rmean , -K(Q)/rmean, 0, u, K(Q)*u/rmean;
		//B.row(Dim-1) << K*a1*a2*(mygamma[0]-mygamma[1]) / (a1*mygamma[0]+a2*mygamma[1]) , 0, 0, 0, 0, u;
	}
}


/** Консервативный поток */
void flux (VectorXd  Q  ,  VectorXd& F ) {
	//F<<Q; return;
	if (MODEL_ID == 7) {
		int Sh = 4;
		for (int phase=0; phase<2; phase++) {
			int shift=Sh*phase;
			ftype u=Q(shift+1)/Q(shift);
			for (int s=0; s<Sh-1; s++) F(s+shift)=Q(s+shift) * u;// * frac;
			ftype pre=pressure(Q , phase);
			F(1+shift)+=pre * frac(Q,phase);
			F(2+shift)+=u*pre * frac(Q,phase);
		}
		F(Sh-1)=0;
		return;
	}
	if (MODEL_ID == 6) {
		ftype u=vel(Q), pmean = pressure(Q,0)*frac(Q,0) + pressure(Q,1)*frac(Q,1);
		for (int s=0; s<6; s++) F(s)=u * Q(s);
		F(1)+= pmean;
		F(2)+=frac(Q,0)*u*pressure(Q,0);
		F(3)=0;
		F(5)+=frac(Q,1)*u*pressure(Q,1);
		return;
	}
	if (MODEL_ID == 5) {
		ftype u=vel(Q), p = p5(Q);
		for (int s=0; s<5; s++) F(s)=u * Q(s);
		F(1)+= p;
		F(2)+=u*p;
		F(3)=0;
		return;
	}    
}

ftype c6(VectorXd Q) {
	ftype f1=frac(Q,0), f2=frac(Q,1), rmean=Q(0)+Q(4);
	ftype cc =  ( mygamma[0]*(pressure(Q,0) + Pref[0]) * f1 + mygamma[1]*(pressure(Q,1) + Pref[1]) * f2 )/rmean ;
	if (cc<small_number) return small_number;
	return sqrt( cc );
}
ftype cw(VectorXd Q) {
	ftype r1=dens(Q,0), f1=frac(Q,0), r2=dens(Q,1), f2=frac(Q,1), rmean=Q(0)+Q(4);
	//ftype cc1=a_a(Q,0), cc2=a_a(Q,1);
	ftype p = p5(Q);
	ftype cc1 = mygamma[0]*(p + Pref[0])/r1, cc2 = mygamma[1]*(p+Pref[1])/r2;
	return sqrt( 1/( (f1/(r1*cc1) + f2/(r2*cc2) ) * rmean ) );
}
/** Скорости распространения волн (собственные значения) */
ftype minmaxeigenvalue(VectorXd Q, int mx) {
	if (MODEL_ID==7) {
		// вычисление скорости звука (идеальный газ) 
		ftype c1=sqrt( a_a(Q,0) ), 
		c2=sqrt( a_a(Q,1) );
		ftype v1 = Q(1)/Q(0), v2 = Q(5)/Q(4);
		if (!mx)
			return min(v1-c1, v2-c2);
		return max(v1+c1, v2+c2);
	} 
	ftype u=vel(Q);
	if (MODEL_ID==6) {
		ftype c=c6(Q);
		if (!mx) return u-c;
		return u+c;
	}
	if (MODEL_ID==5) {
		ftype c = cw(Q);
		if (!mx) return u-c;
		return u+c;
	}
}

ftype ccI (VectorXd Q,int phase) {
	return (pI(Q)/(dens(Q,phase)*dens(Q,phase)) - de_dr(Q,phase))/de_dp(Q,phase);
}

