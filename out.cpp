#include "hllem.hpp"

void numerical_method::out(){
	std::cout << "# rho_s" << std::endl;
	for (int n=0; n<N; n++){
		std::cout << (L*(n+0.5))/N << " " << dens(U[n],0) << std::endl;
	}
	std::cout << "\n" << std::endl;
	
	std::cout << "# rho_g" << std::endl;
	for (int n=0; n<N; n++){
		std::cout << (L*(n+0.5))/N << " " << dens(U[n],1) << std::endl;
	}
	std::cout << "\n" << std::endl;
	
	std::cout << "# u_s" << std::endl;
	for (int n=0; n<N; n++){
		std::cout << (L*(n+0.5))/N << " " << vel( U[n] , 0)<< std::endl;
	}
	std::cout << "\n" << std::endl;
	
	std::cout << "# u_g\n" << std::endl;
	for (int n=0; n<N; n++){
		std::cout << (L*(n+0.5))/N << " " << vel( U[n] , 1) << std::endl;
	}
	std::cout << "\n" << std::endl;
	if (MODEL_ID > 5) {
	std::cout << "# p_s" << std::endl;
	for (int n=0; n<N; n++){
		std::cout << (L*(n+0.5))/N << " " << pressure(U[n],0)<< std::endl;
	}
	std::cout << "\n" << std::endl;
	
	std::cout << "# p_g" << std::endl;
	for (int n=0; n<N; n++){
		std::cout << (L*(n+0.5))/N << " " << pressure(U[n],1)<< std::endl;
	}
	std::cout << "\n" << std::endl;
	
	
	std::cout << "# as * ps + ag * pg" << std::endl;
	for (int n=0; n<N; n++){
		std::cout << (L*(n+0.5))/N << " " <<frac(U[n],0)*pressure(U[n],0)+frac(U[n],1)*pressure(U[n],1)<< std::endl;
	}
	std::cout << "\n" << std::endl;
	}
	
	else {
		std::cout << "# p5" << std::endl;
		for (int n=0; n<N; n++){
			std::cout << (L*(n+0.5))/N << " " << p5(U[n])<< std::endl;
		}
		std::cout << "\n" << std::endl;
	}
	std::cout << "# a_s" << std::endl;
	for (int n=0; n<N; n++){
		std::cout << (L*(n+0.5))/N << " " << U[n](3) << std::endl;
	}
	std::cout << "\n" << std::endl;
	
	
	std::cout << "# slope[0]" << std::endl;
	for (int n=0; n<N; n++){
		std::cout << (L*(n+0.5))/N << " " << slope[n](0) << std::endl;
	}
	std::cout << "\n" << std::endl;
	
	
	std::cout << "# its" << std::endl;
	for (int n=0; n<N; n++){
		std::cout << (L*(n+0.5))/N << " " <<int(its[n])<< std::endl;
	}	
}
