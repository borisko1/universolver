set term pdfcairo size 25,15 font ',70' enhanced
set grid
 set format y "%g"
#set title "N=16000, modelt comparison"
set xlabel "x, a.u."
set ylabel "p, a.u." ; set logscale y

set output "pressure567_N16000.pdf"

plot [][1e4:] "Test6_HLLEM/BN7_relax_N16000.txt" i 4 w l lw 5    t "BN7",\
"Test6_HLLEM/BN6_relax_N16000.txt" i 4 w l lw 5    t "BN6",\
"Test6_HLLEM/BN5_N16000.txt" i 4 w l lw 5    t "BN5",\
"Asy/Press_anal.dat" w l  lw 5  t "Аналитика"

unset logscale

set key left width -9.5
set output "vel567_N16000.pdf"

set ylabel "{/Symbol r}_s, a.u." 

plot [][] "Test6_HLLEM/BN7_relax_N16000.txt" i 3 w l lw 5    t "BN7",\
"Test6_HLLEM/BN6_relax_N16000.txt" i 3 w l lw 5    t "BN6",\
"Test6_HLLEM/BN5_N16000.txt" i 3 w l lw 5    t "BN5",\
"Asy/Vel_anal.dat" w l  lw 5 t "Аналитика"

set key left 
set output "frac567_N16000.pdf"

set ylabel "{/Symbol a}_s, a.u." 
set logscale y
plot [][] "Test6_HLLEM/BN7_relax_N16000.txt" i 6 w l lw 5    t "BN7",\
"Test6_HLLEM/BN6_relax_N16000.txt" i 6 w l lw 5   t "BN6",\
"Test6_HLLEM/BN5_N16000.txt" i 5 w l lw 5    t "BN5",\
"Asy/Phi_anal.dat" w l  lw 5 t "Аналитика"


set key right
set output "pressure4000.pdf"

set ylabel "p, a.u." 
plot [][1e4:] \
"Test6_HLLEM/BN7_relax_N4000.txt" i 2 w l lw 5  t "BN7, N=4000",\
"Test6_HLLEM/BN6_relax_N4000.txt" i 1 w l lw 5  t "BN6, N=4000",\
"Test6_HLLEM/BN5_N4000.txt" i 4 w l lw 5  t "BN5, N=4000",\
"Asy/Press_anal.dat" w l  lw 5  t "Аналитика"

set output "pressure16000.pdf"
plot [][1e4:] \
"Test6_HLLEM/BN7_relax_N16000.txt" i 4 w l lw 5  t "BN7, N=16000",\
"Test6_HLLEM/BN6_relax_N16000.txt" i 4 w l lw 5  t "BN6, N=16000",\
"Test6_HLLEM/BN5_N16000.txt" i 4 w l lw 5  t "BN5, N=16000",\
"Asy/Press_anal.dat" w l  lw 5  t "Аналитика"
